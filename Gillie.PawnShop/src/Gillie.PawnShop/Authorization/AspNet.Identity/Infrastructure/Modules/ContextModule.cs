﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Context;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Modules
{
    public class ContextModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<PawnshopContext>()
                .As<DbContext>()
                .InstancePerLifetimeScope();
        }
    }
}
