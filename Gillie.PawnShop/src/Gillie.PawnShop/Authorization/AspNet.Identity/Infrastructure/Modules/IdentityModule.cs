﻿using Autofac;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository.Interfaces;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRepository;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRepository.Interfaces;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository.Interfaces;
using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Context;
using Microsoft.AspNetCore.Identity;

namespace Gillie.PawnShop.Authorization.AspNet.Infrastructure.Modules
{
    public class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PawnshopContext>();
            builder.RegisterType<UserManager<IdentityUser>>();
            builder.RegisterType<SignInManager<IdentityUser>>();

            builder.RegisterType<UserReadRepository>().As<IUserReadRepository>();
            builder.RegisterType<RoleReadRepository>().As<IRoleReadRepository>();
            builder.RegisterType<UserRoleReadRepository>().As<IUserRoleReadRepository>();
        }
    }
}
