﻿using System.Threading.Tasks;
using Gillie.PawnShop.Authorization.Entities.Identity;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRepository.Interfaces
{
    public interface IUserReadRepository
    {
        Task<IdentityUser[]> SearchUsersAsync(string query, int? limit);
        Task<IdentityUser[]> GetAllUsers(int? limit);
        Task<IdentityUser> GetUserAsync(long id);
    }
}
