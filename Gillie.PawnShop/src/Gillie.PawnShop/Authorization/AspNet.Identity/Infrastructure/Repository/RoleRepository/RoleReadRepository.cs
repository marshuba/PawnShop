﻿using System.Threading.Tasks;
using Gillie.PawnShop.Authorization.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository.Interfaces;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository
{
    public class RoleReadRepository : IRoleReadRepository
    {
        private readonly DbSet<IdentityRole> identityRoles;

        public RoleReadRepository(DbContext context)
        {
            identityRoles = context.Set<IdentityRole>();
        }

        public Task<IdentityRole[]> GetRolesAsync()
        {
            return identityRoles.ToArrayAsync();
        }
    }
}
