﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository.Interfaces
{
    public interface IRoleReadRepository
    {
        Task<IdentityRole[]> GetRolesAsync();
    }
}
