﻿using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Authorization.Entities.Identity;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository
{
    public class UserRoleReadRepository : IUserRoleReadRepository
    {
        private readonly DbSet<IdentityUserRole> usersAndRoles;

        public UserRoleReadRepository(DbContext context)
        {
            usersAndRoles = context.Set<IdentityUserRole>();
        }

        public Task<IdentityUserRole[]> GetUserAndRolesAsync()
        {
            return usersAndRoles.ToArrayAsync();
        }
    }
}
