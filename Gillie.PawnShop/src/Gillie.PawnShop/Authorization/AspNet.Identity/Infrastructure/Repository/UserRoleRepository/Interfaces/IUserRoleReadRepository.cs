﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository.Interfaces
{
    public interface IUserRoleReadRepository
    {
        Task<IdentityUserRole[]> GetUserAndRolesAsync();
    }
}
