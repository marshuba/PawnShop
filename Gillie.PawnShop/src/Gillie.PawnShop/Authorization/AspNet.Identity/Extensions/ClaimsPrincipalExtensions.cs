﻿using System.Linq;
using System.Security.Claims;

namespace Gillie.PawnShop.Authorization.AspNet.Identity.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetDisplayName(this ClaimsPrincipal claimsPrincipal)
        {
            var name = claimsPrincipal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.GivenName);
            if (name == null)
                return string.Empty;

            var surname = claimsPrincipal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Surname);
            if (surname == null)
                return string.Empty;

            return $"{name.Value} {surname.Value}";
        }
    }
}
