﻿namespace Gillie.PawnShop.Authorization.Entities.Enums
{
    public enum AccountType
    {
        Admin = 1, Employee, Client
    }
}
