﻿namespace Gillie.PawnShop.Authorization.Entities.Identity
{
    public class IdentityUserRole
    {
        public long UserId { get; set; }
        public long RoleId { get; set; }
    }
}
