﻿using Gillie.PawnShop.Repository.BranchLocationRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.BranchLocationRepository
{
    public class BranchLocationWriteRepository : IBranchLocationWriteRepository
    {
        private readonly DbSet<BranchLocation> branchLocations;
        private readonly DbContext context;

        public BranchLocationWriteRepository(DbContext context)
        {
            this.context = context;
            branchLocations = context.Set<BranchLocation>();
        }

        public Task AddBranchLocationAsync(BranchLocation location)
        {
            branchLocations.Add(location);
            return context.SaveChangesAsync();
        }

        public Task RemoveBranchLocationAsync(BranchLocation location)
        {
            branchLocations.Remove(location);
            return context.SaveChangesAsync();
        }
    }
}
