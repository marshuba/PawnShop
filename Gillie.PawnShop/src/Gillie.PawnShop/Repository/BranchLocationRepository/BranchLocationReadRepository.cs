﻿using Gillie.PawnShop.Repository.BranchLocationRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.BranchLocationRepository
{
    public class BranchLocationReadRepository : IBranchLocationReadRepository
    {
        private readonly DbSet<BranchLocation> branchLocations;

        public BranchLocationReadRepository(DbContext context)
        {
            branchLocations = context.Set<BranchLocation>();
        }

        public Task<BranchLocation[]> GetBranchLocationsAsync()
        {
            return branchLocations.ToArrayAsync();
        }
    }
}
