﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchLocationRepository.Interfaces
{
    public interface IBranchLocationReadRepository
    {
        Task<BranchLocation[]> GetBranchLocationsAsync();
    }
}
