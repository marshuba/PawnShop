﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchLocationRepository.Interfaces
{
    public interface IBranchLocationWriteRepository
    {
        Task AddBranchLocationAsync(BranchLocation location);
        Task RemoveBranchLocationAsync(BranchLocation location);
    }
}
