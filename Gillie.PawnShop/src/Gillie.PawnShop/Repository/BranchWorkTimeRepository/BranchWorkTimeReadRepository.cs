﻿using Gillie.PawnShop.Repository.BranchWorkTimeRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.BranchWorkTimeRepository
{
    public class BranchWorkTimeReadRepository : IBranchWorkTimeReadRepository
    {
        private readonly DbSet<BranchWorkTime> branchWorkTimes;

        public BranchWorkTimeReadRepository(DbContext context)
        {
            branchWorkTimes = context.Set<BranchWorkTime>();
        }

        public Task<BranchWorkTime[]> GetBranchWorkTimesAsync()
        {
            return branchWorkTimes.ToArrayAsync();
        }
    }
}
