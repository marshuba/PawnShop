﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchWorkTimeRepository.Interfaces
{
    public interface IBranchWorkTimeWriteRepository
    {
        Task AddBranchWorkTimeAsync(BranchWorkTime workTime);
        Task RemoveBranchWorkTimeAsync(BranchWorkTime workTime);
        Task UpdateBranchWorkTimeAsync(BranchWorkTime workTime);
    }
}
