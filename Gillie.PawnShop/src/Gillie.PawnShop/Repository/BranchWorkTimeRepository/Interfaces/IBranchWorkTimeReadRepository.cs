﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchWorkTimeRepository.Interfaces
{
    public interface IBranchWorkTimeReadRepository
    {
        Task<BranchWorkTime[]> GetBranchWorkTimesAsync();
    }
}
