﻿using Gillie.PawnShop.Repository.BranchWorkTimeRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Gillie.PawnShop.Repository.BranchWorkTimeRepository
{
    public class BranchWorkTimeWriteRepository : IBranchWorkTimeWriteRepository
    {
        private readonly DbSet<BranchWorkTime> branchWorkTimes;
        private readonly DbContext context;

        public BranchWorkTimeWriteRepository(DbContext context)
        {
            this.context = context;
            branchWorkTimes = context.Set<BranchWorkTime>();
        }

        public Task AddBranchWorkTimeAsync(BranchWorkTime workTime)
        {
            branchWorkTimes.Add(workTime);
            return context.SaveChangesAsync();
        }

        public Task RemoveBranchWorkTimeAsync(BranchWorkTime workTime)
        {
            branchWorkTimes.Remove(workTime);
            return context.SaveChangesAsync();
        }

        public Task UpdateBranchWorkTimeAsync(BranchWorkTime workTime)
        {
            var temp = branchWorkTimes.Where(w => w.BranchWorkTimeId == workTime.BranchWorkTimeId).
                FirstOrDefault();

            temp = workTime;
            context.Entry(temp).State = EntityState.Modified;
            return context.SaveChangesAsync();
        }
    }
}
