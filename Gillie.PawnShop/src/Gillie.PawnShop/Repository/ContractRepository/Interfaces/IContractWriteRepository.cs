﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ContractRepository.Interfaces
{
    public interface IContractWriteRepository
    {
        Task AddContract(Contract contract);
        Task RemoveContract(Contract contract);
        Task CloseContractAsync(Contract contract);
    }
}
