﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Responce;
using System;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ContractRepository.Interfaces
{
    public interface IContractReadRepository
    {
        Task<Contract[]> GetContractsAsync();
        Task<Contract[]> GetContractsByClientIdAsync(int clientId);
        Task<Contract> GetContractByContractIdAsync(long contractId);
        Task<long> GetContractWithLastId();

        Task<ContractPawnItemResponce[]> GetContractsForEmployeeReport(int clientId);
        Task<Contract[]> GetClosedContractsByDatas(DateTime firstDate, DateTime lastDate);
        Task<Contract[]> GetContractsByClientAsync(Client client);
        Task<Contract[]> GetContractsByEmployeeAsync(Employee employee);
        Task<Contract[]> GetContractsByEmployeeAsync(Employee employee, DateTime firstDate, DateTime lastDate);
    }
}