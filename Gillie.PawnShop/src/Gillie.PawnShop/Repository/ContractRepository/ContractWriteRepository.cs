﻿using Gillie.PawnShop.Repository.ContractRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ContractRepository
{
    public class ContractWriteRepository : IContractWriteRepository
    {
        private DbSet<Contract> contracts;
        private DbContext context;

        public ContractWriteRepository(DbContext context)
        {
            this.context = context;
            contracts = context.Set<Contract>();
        }

        public Task AddContract(Contract contract)
        {
            contracts.Add(contract);
            return context.SaveChangesAsync();
        }

        public Task RemoveContract(Contract contract)
        {
            contracts.Remove(contract);
            return context.SaveChangesAsync();
        }

        public async Task CloseContractAsync(Contract contract)
        {
            contracts.Update(contract);
            await context.SaveChangesAsync();
        }
    }
}
