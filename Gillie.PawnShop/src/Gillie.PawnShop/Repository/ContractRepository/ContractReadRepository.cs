﻿using Gillie.PawnShop.Repository.ContractRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using Gillie.PawnShop.Responce;

namespace Gillie.PawnShop.Repository.ContractRepository
{
    public class ContractReadRepository : IContractReadRepository
    {
        private readonly DbSet<Contract> contracts;

        public ContractReadRepository(DbContext context)
        {
            contracts = context.Set<Contract>();
        }

        public Task<Contract[]> GetContractsAsync()
        {
            return contracts.ToArrayAsync();
        }

        public Task<Contract[]> GetContractsByClientIdAsync(int clientId)
        {
            return contracts.Where(contract => contract.ClientId == clientId && contract.IsClosed == false).ToArrayAsync();
        }
        
        public Task<ContractPawnItemResponce[]> GetContractsForEmployeeReport(int clientId)
        {
            return contracts.Where(contract => contract.ClientId == clientId)
                .Select(contract => new ContractPawnItemResponce()
                {
                    Contract = contract,
                    PawnItem = contract.PawnItem
                }).ToArrayAsync();
        }

        public Task<Contract> GetContractByContractIdAsync(long contractId)
        {
            return contracts.Include(contract => contract.PawnItem).FirstOrDefaultAsync(contract => contract.ContractId == contractId);
        }

        public Task<long> GetContractWithLastId()
        {
            return contracts.LongCountAsync();
        }

        public async Task<Contract[]> GetClosedContractsByDatas(DateTime firstDate, DateTime lastDate)
        {
            return await contracts.Where(
                contract =>
                IsContractInDateRange(contract, firstDate, lastDate))
                .ToArrayAsync();
        }

        public async Task<Contract[]> GetContractsByClientAsync(Client client)
        {
            return await contracts.Where(
                contract => contract.ClientId == client.ClientId).ToArrayAsync();
        }

        public async Task<Contract[]> GetContractsByEmployeeAsync(Employee employee)
        {
            return await contracts.Where(
                contract => contract.EmployeeId == employee.EmployeeId).ToArrayAsync();
        }

        public async Task<Contract[]> GetContractsByEmployeeAsync(Employee employee, DateTime firstDate, DateTime lastDate)
        {
            return await contracts.Where(
                contract =>
                contract.EmployeeId == employee.EmployeeId &&
                IsContractInDateRange(contract, firstDate, lastDate))
                .ToArrayAsync();
        }

        private bool IsContractInDateRange(Contract contract, DateTime firstDate, DateTime lastDate)
        {
            return contract.DateStart >= firstDate && contract.DateEnd <= lastDate;
        }
    }
}