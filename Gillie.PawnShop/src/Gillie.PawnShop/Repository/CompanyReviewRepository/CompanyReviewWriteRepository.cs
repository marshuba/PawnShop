﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.CompanyReviewRepository
{
    public class CompanyReviewWriteRepository : ICompanyReviewWriteRepository
    {
        private readonly DbSet<CompanyReview> companyReviews;
        private readonly DbContext context;

        public CompanyReviewWriteRepository(DbContext context)
        {
            this.context = context;
            companyReviews = context.Set<CompanyReview>();
        }

        public Task AddAsyncCompanyReview(CompanyReview companyReview)
        {
            companyReviews.Add(companyReview);
            return context.SaveChangesAsync();
        }

        public async Task RemoveAsyncCompanyReview(long companyReviewId)
        {
            var review = new CompanyReview() { CompanyReviewId = companyReviewId };
            companyReviews.Attach(review);
            companyReviews.Remove(review);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsyncCompanyReview(CompanyReview companyReview)
        {
            companyReviews.Update(companyReview);
            await context.SaveChangesAsync();
        }
    }
}
