﻿using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Responce;

namespace Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces
{
    public interface ICompanyReviewReadRepository
    {
        Task<CompanyReviewResponce[]> GetPublishedCompanyReviewAsync();
        Task<CompanyReviewResponce[]> GetAllCompanyReviewAsync();
        Task<CompanyReview> GetCompanyReviewByIdAsync(long companyReviewId);
    }
}
