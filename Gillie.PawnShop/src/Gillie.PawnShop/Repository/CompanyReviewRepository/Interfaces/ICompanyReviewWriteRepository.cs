﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces
{
    public interface ICompanyReviewWriteRepository
    {
        Task AddAsyncCompanyReview(CompanyReview companyReview);
        Task RemoveAsyncCompanyReview(long companyReviewId);
        Task UpdateAsyncCompanyReview(CompanyReview companyReview);
    }
}
