﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces;
using Gillie.PawnShop.Responce;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.CompanyReviewRepository
{
    public class CompanyReviewReadRepository : ICompanyReviewReadRepository
    {
        private readonly DbSet<CompanyReview> companyReviews;
        private readonly DbSet<Client> clients;

        public CompanyReviewReadRepository(DbContext context)
        {
            companyReviews = context.Set<CompanyReview>();
            clients = context.Set<Client>();
        }

        public Task<CompanyReview> GetCompanyReviewByIdAsync(long companyReviewId)
        {
            return companyReviews.FirstOrDefaultAsync(review => review.CompanyReviewId == companyReviewId);
        }

        public Task<CompanyReviewResponce[]> GetPublishedCompanyReviewAsync()
        {
            var anonimousReviewResponce = from review in companyReviews
                                          where review.Publish == true
                                          orderby review.ReviewDate descending
                                          join client in clients on review.ClientId equals client.ClientId
                                          select new CompanyReviewResponce
                                          {
                                              ReviewDate = review.ReviewDate,
                                              Description = review.Description,
                                              Name = client.Name,
                                              Surname = client.Surname,
                                          };

            return anonimousReviewResponce.ToArrayAsync();
        }

        public Task<CompanyReviewResponce[]> GetAllCompanyReviewAsync()
        {
            var anonimousReviewResponce = from review in companyReviews
                                          orderby review.ReviewDate descending
                                          join client in clients on review.ClientId equals client.ClientId
                                          select new CompanyReviewResponce
                                          {
                                              CompanyReviewId = review.CompanyReviewId,
                                              ReviewDate = review.ReviewDate,
                                              Description = review.Description,
                                              Publish = review.Publish,

                                              ClientId = client.ClientId,
                                              Name = client.Name,
                                              Surname = client.Surname,
                                              Email = client.Email,
                                              PhoneNumber = client.PhoneNumber
                                          };

            return anonimousReviewResponce.ToArrayAsync();
        }
    }
}
