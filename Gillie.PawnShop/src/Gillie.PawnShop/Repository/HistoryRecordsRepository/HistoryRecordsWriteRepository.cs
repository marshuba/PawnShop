﻿using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces;

namespace Gillie.PawnShop.Repository.HistoryRecordsRepository
{
    public class HistoryRecordsWriteRepository : IHistoryRecordsWriteRepository
    {
        private readonly DbSet<HistoryRecord> historyRecords;
        private readonly DbContext context;

        public HistoryRecordsWriteRepository(DbContext context)
        {
            this.context = context;
            historyRecords = context.Set<HistoryRecord>();
        }

        public Task AddAsyncHistoryRecords(HistoryRecord historyRecord)
        {
            historyRecords.Add(historyRecord);
            return context.SaveChangesAsync();
        }

        public async Task RemoveAsyncHistoryRecords(long historyRecord)
        {
            var record = new HistoryRecord() {HistoryRecordId = historyRecord};
            historyRecords.Attach(record);
           historyRecords.Remove(record);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsyncHistoryRecords (HistoryRecord historyRecord)
        {
            historyRecords.Update(historyRecord);
            await context.SaveChangesAsync();
        }
    }
}
