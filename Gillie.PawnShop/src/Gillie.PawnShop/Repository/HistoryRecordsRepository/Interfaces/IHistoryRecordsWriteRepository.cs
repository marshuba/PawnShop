﻿using System.Threading.Tasks;
using Gillie.PawnShop.Entities;

namespace Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces
{
    public interface IHistoryRecordsWriteRepository
    {
        Task AddAsyncHistoryRecords(HistoryRecord historyRecord);
        Task RemoveAsyncHistoryRecords(long historyRecordId);
        Task UpdateAsyncHistoryRecords(HistoryRecord historyRecord);
    }
}
