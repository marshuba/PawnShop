﻿using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces
{
    public interface IHistoryRecordsReadRepository
    {
        Task<HistoryRecord[]> GetAllHistoryRecordsAsync(RecordType recordType);
        Task<HistoryRecord[]> GetAllHistoryRecordsAsync();
        Task<HistoryRecord[]> GetHistoryRecordAsync(int recordId);
    }
}
