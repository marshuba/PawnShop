﻿using System.Linq;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces;
using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Repository.HistoryRecordsRepository
{
    class HistoryRecordsReadRepository : IHistoryRecordsReadRepository
    {
        private readonly DbSet<HistoryRecord> historyRecords;

        public HistoryRecordsReadRepository(DbContext context)
        {
            historyRecords = context.Set<HistoryRecord>();
        }

        public Task<HistoryRecord[]> GetAllHistoryRecordsAsync(RecordType recordType)
        { 
            return historyRecords.Where(record => record.HistoryRecordTypeId == (int)recordType).OrderBy(record => record.HistoryRecordDate).ToArrayAsync();
        }
               
        public Task<HistoryRecord[]> GetAllHistoryRecordsAsync()
        {
            return historyRecords.ToArrayAsync();
        }

        public Task<HistoryRecord[]> GetHistoryRecordAsync(int recordId)
        {
            return historyRecords.Where(record => record.HistoryRecordTypeId == recordId).ToArrayAsync();
        }
    }
}
