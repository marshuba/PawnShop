﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.PawnItemRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.PawnItemRepository
{
    public class PawnItemWriteRepository : IPawnItemWriteRepository
    {
        private DbSet<PawnItem> pawnItems;
        private DbContext context;

        public PawnItemWriteRepository(DbContext context)
        {
            this.context = context;
            pawnItems = context.Set<PawnItem>();
        }

        public async Task ChangePawnItemStateOnReturnAsync(PawnItem pawnItem)
        {
            pawnItems.Update(pawnItem);
            await context.SaveChangesAsync();
        }

        public Task AddPawnItem(PawnItem pawnItem)
        {
            pawnItems.Add(pawnItem);
            return context.SaveChangesAsync();
        }

        public Task UpdatePawnItem(PawnItem item)
        {
            pawnItems.Update(item);
            return context.SaveChangesAsync();
        }
    }
}
