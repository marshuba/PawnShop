﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.PawnItemRepository.Interfaces
{
    public interface IPawnItemReadRepository
    {
        Task<PawnItem[]> GetPawnItemsAsync();
        Task<PawnItem> GetPawnItemByIdAsync(long pawnItemId);
        Task<PawnItem> GetPawnItemByContractAsync(Contract contract);
        Task<PawnItem[]> GetPawnItemsByContractsAsync(Contract[] contracts);
        Task<PawnItem[]> GetPawnItemsOnStockAsync();
    }
}
