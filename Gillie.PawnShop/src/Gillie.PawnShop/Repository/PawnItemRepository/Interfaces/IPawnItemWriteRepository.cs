﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.PawnItemRepository.Interfaces
{
    public interface IPawnItemWriteRepository
    {
        Task ChangePawnItemStateOnReturnAsync(PawnItem pawnItem);
        Task AddPawnItem(PawnItem pawnItem);
        Task UpdatePawnItem(PawnItem item);
    }
}
