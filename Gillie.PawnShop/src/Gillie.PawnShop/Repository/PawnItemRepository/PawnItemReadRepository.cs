﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Entities.Enums;
using Gillie.PawnShop.Repository.PawnItemRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.PawnItemRepository
{
    public class PawnItemReadRepository : IPawnItemReadRepository
    {
        private readonly DbSet<PawnItem> pawnItems;

        public PawnItemReadRepository(DbContext context)
        {
            pawnItems = context.Set<PawnItem>();
        }

        public Task<PawnItem[]> GetPawnItemsBelongShopAsync()
        {
            return pawnItems.Where(pawnItem => pawnItem.PawnItemState == PawnItemState.OnStockBelongShop).ToArrayAsync();
        }

        public Task<PawnItem> GetPawnItemByIdAsync(long pawnItemId)
        {
            return pawnItems.FirstOrDefaultAsync(pawnItem => pawnItem.PawnItemId == pawnItemId);
        }

        public async Task<PawnItem> GetPawnItemByContractAsync(Contract contract)
        {
            return await pawnItems.FirstOrDefaultAsync(
                pawnItem => pawnItem.ContractId == contract.ContractId);
        }

        public async Task<PawnItem[]> GetPawnItemsAsync()
        {
            return await pawnItems.ToArrayAsync();
        }

        public async Task<PawnItem[]> GetPawnItemsByContractsAsync(Contract[] contracts)
        {
            return await pawnItems.Join(
                contracts, pawnItem =>
                pawnItem.ContractId, contract => contract.ContractId,
                (pawnItem, contract) => pawnItem).ToArrayAsync();
        }

        public async Task<PawnItem[]> GetPawnItemsOnStockAsync()
        {
            return await pawnItems.Where(
                pawnItem =>
                pawnItem.PawnItemState == PawnItemState.OnStockBelongShop ||
                pawnItem.PawnItemState == PawnItemState.OnStockBelongClient)
                .ToArrayAsync();
        }
    }
}