﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.EmployeeRepository.Interfaces
{
    public interface IEmployeeWriteRepository
    {
        Task Add(Employee employee);
        Task Remove(Employee employee);
        Task Update(Employee employee);
    }
}
