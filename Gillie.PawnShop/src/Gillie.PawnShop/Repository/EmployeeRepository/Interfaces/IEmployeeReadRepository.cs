﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.EmployeeRepository.Interfaces
{
    public interface IEmployeeReadRepository
    {
        Task<Employee[]> GetEmployeesAsync();
        Task<Employee> GetEmployeeByEmailAsync(string email);
    }
}
