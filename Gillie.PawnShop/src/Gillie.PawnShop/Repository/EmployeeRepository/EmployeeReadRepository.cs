﻿using Gillie.PawnShop.Repository.EmployeeRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.EmployeeRepository
{
    public class EmployeeReadRepository : IEmployeeReadRepository
    {
        private readonly DbSet<Employee> employees;

        public EmployeeReadRepository(DbContext context)
        {
            employees = context.Set<Employee>();
        }

        public async Task<Employee> GetEmployeeByEmailAsync(string email)
        {
            return await employees.FirstOrDefaultAsync(employee => employee.Email == email);
        }

        public async Task<Employee[]> GetEmployeesAsync()
        {
            return await employees.ToArrayAsync();
        }
    }
}