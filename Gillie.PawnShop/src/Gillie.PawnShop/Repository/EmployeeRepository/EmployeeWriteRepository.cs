﻿using System.Linq;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Repository.EmployeeRepository.Interfaces;

namespace Gillie.PawnShop.Repository.EmployeeRepository
{
    public class EmployeeWriteRepository : IEmployeeWriteRepository
    {
        private readonly DbSet<Employee> employees;
        private readonly DbContext context;

        public EmployeeWriteRepository(DbContext context)
        {
            this.context = context;
            employees = context.Set<Employee>();
        }

        public Task Add(Employee employee)
        {
            employees.Add(employee);
            return context.SaveChangesAsync();
        }

        public Task Remove(Employee employee)
        {
            employees.Remove(employee);
            return context.SaveChangesAsync();
        }

        public Task Update(Employee employee)
        {
            var temp = employees.Where(e => e.EmployeeId == employee.EmployeeId).FirstOrDefault();
            temp = employee;
            context.Entry(temp).State = EntityState.Modified;
            return context.SaveChangesAsync();
        }
    }
}
