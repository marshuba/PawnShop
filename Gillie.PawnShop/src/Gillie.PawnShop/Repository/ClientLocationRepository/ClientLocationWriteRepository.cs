﻿using Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ClientLocationRepository
{
    public class ClientLocationWriteRepository : IClientLocationWriteRepository
    {
        private readonly DbSet<ClientLocation> clientLocations;
        private readonly DbContext context;

        public ClientLocationWriteRepository(DbContext context)
        {
            this.context = context;
            clientLocations = context.Set<ClientLocation>();
        }

        public Task AddClientLocation(ClientLocation clientLocation)
        {
            clientLocations.Add(clientLocation);
            return context.SaveChangesAsync();
        }

        public Task RemoveClientLocation(ClientLocation clientLocation)
        {
            clientLocations.Remove(clientLocation);
            return context.SaveChangesAsync();
        }

        public Task UpdateClientLocation(ClientLocation clientLocation)
        {
            var temp = clientLocations.Where(thisclientLocation => thisclientLocation.ClientLocationId == clientLocation.ClientLocationId)
                .FirstOrDefault();
            temp = clientLocation;
            context.Entry<ClientLocation>(temp).State = EntityState.Modified;
            return context.SaveChangesAsync();
        }
    }
}
