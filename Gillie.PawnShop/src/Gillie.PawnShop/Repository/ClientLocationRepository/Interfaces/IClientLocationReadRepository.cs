﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces
{
    public interface IClientLocationReadRepository
    {
        Task<ClientLocation[]> GetClientLocationsAsync();
    }
}
