﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces
{
    public interface IClientLocationWriteRepository
    {
        Task AddClientLocation(ClientLocation clientLocation);
        Task RemoveClientLocation(ClientLocation clientLocation);
        Task UpdateClientLocation(ClientLocation clientLocation);
    }
}
