﻿using Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces;
using Gillie.PawnShop.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ClientLocationRepository
{
    public class ClientLocationReadRepository : IClientLocationReadRepository
    {
        private readonly DbSet<ClientLocation> clientLocations;

        public ClientLocationReadRepository(DbContext context)
        {
            clientLocations = context.Set<ClientLocation>();
        }

        public Task<ClientLocation[]> GetClientLocationsAsync()
        {
            return clientLocations.ToArrayAsync();
        }
    }
}
