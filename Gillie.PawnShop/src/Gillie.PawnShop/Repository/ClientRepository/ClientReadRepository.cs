﻿using Gillie.PawnShop.Repository.ClientRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ClientRepository
{
    public class ClientReadRepository : IClientReadRepository
    {
        private readonly DbSet<Client> clients;

        public ClientReadRepository(DbContext context)
        {
            clients = context.Set<Client>();
        }

        public async Task<Client> GetClientByEmailAsync(string email)
        {
            return await clients.FirstOrDefaultAsync(client => client.Email == email);
        }

        public async Task<Client[]> GetClientsAsync()
        {
            return await clients.ToArrayAsync();
        }
    }
}