﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientRepository.Interfaces
{
    public interface IClientReadRepository
    {
        Task<Client[]> GetClientsAsync();
        Task<Client> GetClientByEmailAsync(string email);
    }
}
