﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientRepository.Interfaces
{
    public interface IClientWriteRepository
    {
        Task Add(Client client);
        Task Remove(Client client);
        Task Update(Client client);
    }
}
