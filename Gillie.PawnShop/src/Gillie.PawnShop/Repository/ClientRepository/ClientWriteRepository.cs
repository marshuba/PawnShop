﻿using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;

namespace Gillie.PawnShop.Repository.ClientRepository
{
    public class ClientWriteRepository : IClientWriteRepository
    {
        private readonly DbSet<Client> clients;
        private readonly DbContext context;

        public ClientWriteRepository(DbContext context)
        {
            this.context = context;
            clients = context.Set<Client>();
        }

        public Task Add(Client client)
        {
            clients.Add(client);
            return context.SaveChangesAsync();
        }

        public Task Remove(Client client)
        {
            clients.Remove(client);
            return context.SaveChangesAsync();
        }

        public Task Update(Client client)
        {
            clients.Update(client);
            return context.SaveChangesAsync();
        }
    }
}
