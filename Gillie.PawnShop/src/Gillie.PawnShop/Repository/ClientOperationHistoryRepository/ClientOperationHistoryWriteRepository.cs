﻿using Gillie.PawnShop.Repository.ClientOperationHistoryRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ClientOperationHistoryRepository
{
    public class ClientOperationHistoryWriteRepository : IClientOperationHistoryWriteRepository
    {
        private readonly DbSet<ClientOperationHistory> clientOperHistories;
        private readonly DbContext context;

        public ClientOperationHistoryWriteRepository(DbContext context)
        {
            this.context = context;
            clientOperHistories = context.Set<ClientOperationHistory>();
        }

        public Task AddClientOperationHistory(ClientOperationHistory clientOperHistory)
        {
            clientOperHistories.Add(clientOperHistory);
            return context.SaveChangesAsync();
        }

        public Task RemoveClientOperationHistory(ClientOperationHistory clientOperHistory)
        {
            clientOperHistories.Remove(clientOperHistory);
            return context.SaveChangesAsync();
        }
    }
}
