﻿using Gillie.PawnShop.Repository.ClientOperationHistoryRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.ClientOperationHistoryRepository
{
    public class ClientOperationHistoryReadRepository : IClientOperationHistoryReadRepository
    {
        private readonly DbSet<ClientOperationHistory> clientOperHistories;

        public ClientOperationHistoryReadRepository(DbContext context)
        {
            clientOperHistories = context.Set<ClientOperationHistory>();
        }

        public Task<ClientOperationHistory[]> GetClientOperationHistoriesAsync()
        {
            return clientOperHistories.ToArrayAsync();
        }
    }
}
