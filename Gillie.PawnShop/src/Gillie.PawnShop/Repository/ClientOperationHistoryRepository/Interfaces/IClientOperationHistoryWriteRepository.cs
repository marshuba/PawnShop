﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientOperationHistoryRepository.Interfaces
{
    public interface IClientOperationHistoryWriteRepository
    {
        Task AddClientOperationHistory(ClientOperationHistory clientOperHistory);
        Task RemoveClientOperationHistory(ClientOperationHistory clientOperHistory);
    }
}
