﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.ClientOperationHistoryRepository.Interfaces
{
    public interface IClientOperationHistoryReadRepository
    {
        Task<ClientOperationHistory[]> GetClientOperationHistoriesAsync();
    }
}
