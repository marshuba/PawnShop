﻿using Gillie.PawnShop.Repository.CompanyRepository.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.CompanyRepository
{
    public class CompanyReadRepository : ICompanyReadRepository
    {
        private readonly DbSet<Company> companies;
        
        public CompanyReadRepository(DbContext context)
        {
            companies = context.Set<Company>();
        }

        public Task<Company[]> GetCompaniesAsync()
        {
            return companies.ToArrayAsync();
        }
    }
}
