﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.CompanyRepository.Interfaces
{
    public interface ICompanyReadRepository
    {
        Task<Company[]> GetCompaniesAsync();
    }
}
