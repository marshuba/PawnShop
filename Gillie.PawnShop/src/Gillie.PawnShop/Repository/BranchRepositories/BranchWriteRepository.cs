﻿using Gillie.PawnShop.Repository.BranchRepositories.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.BranchRepositories
{
    public class BranchWriteRepository : IBranchWriteRepository
    {
        private readonly DbSet<Branch> branches;
        private readonly DbContext context;

        public BranchWriteRepository(DbContext context)
        {
            this.context = context;
            branches = context.Set<Branch>();
        }

        public Task AddAsyncBranch(Branch branch)
        {
            branches.Add(branch);
            return context.SaveChangesAsync();
        }

        public async Task RemoveAsyncBranch(long branchId)
        {
            var removeBranch = await branches.FirstOrDefaultAsync(branch => branch.BranchId == branchId);
            branches.Remove(removeBranch);
            await context.SaveChangesAsync();
        }

        public async Task UpdateAsyncBranch(Branch branch)
        {
            branches.Update(branch);
            await context.SaveChangesAsync();
        }
    }
}