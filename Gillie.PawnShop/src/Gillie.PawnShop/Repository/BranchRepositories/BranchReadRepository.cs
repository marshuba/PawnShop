﻿using Gillie.PawnShop.Repository.BranchRepositories.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;

namespace Gillie.PawnShop.Repository.BranchRepositories
{
    class BranchReadRepository : IBranchReadRepository
    {
        private readonly DbSet<Branch> branches;

        public BranchReadRepository(DbContext context)
        {
            branches = context.Set<Branch>();
        }

        public Task<Branch[]> GetAllBranchesAsync()
        {
            return branches.ToArrayAsync();
        }
    }
}
