﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchRepositories.Interfaces
{
    public interface IBranchWriteRepository
    {
        Task AddAsyncBranch(Branch branch);
        Task RemoveAsyncBranch(long branch);
        Task UpdateAsyncBranch(Branch branch);
    }
}
