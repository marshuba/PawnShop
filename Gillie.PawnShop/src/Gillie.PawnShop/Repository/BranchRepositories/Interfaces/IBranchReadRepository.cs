﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Repository.BranchRepositories.Interfaces
{
    public interface IBranchReadRepository
    {
        Task<Branch[]> GetAllBranchesAsync();
    }
}
