﻿(function () {
    "use strict";
    angular
       .module('PawnApp')
       .controller("employeeController", employeeController);
    employeeController.$inject = ['$q', '$scope', 'employeeService', '$http'];
    function employeeController($q, $scope, employeeService, $http) {

        $scope.findClientDiv = false;
        $scope.notFoundDiv = false;
        $scope.addClientDiv = false;
        $scope.addContractDiv = false;
        $scope.showContractDiv = false;
        $scope.notFoundContractDiv = false;
        $scope.showContractForm = false;

        $scope.newClient = {
            ClientId: '',
            Name: '',
            Surname: '',
            Email: '',
            PhoneNumber: ''           
        };

        $scope.newContract = {
            rate: '',
            dateEnd: '',
            pawnItemName: '',
            pawnItemDescription: '',
            buyPrice: '',
            sellPrice: ''
        };

        $scope.contracts = [];

        $scope.registerRequest = {
            email: "",
            password: "",
            confirmPassword: "",
            name: "",
            surname: "",
            phoneNumber: "",
        };

        $scope.createClient = function () {

            if ($scope.registerRequest.Password !=
                $scope.registerRequest.ConfirmPassword) {
                alert('Incorrect passwords');
                return;
            }

            $scope.clientPromise = createClientPromise($scope.registerRequest);
            $scope.clientPromise.then(function () {
                alert("Client successful created");
                $scope.clearForm();
            });
        };

        var createClientPromise = function (registerRequest) {
            return $q(function (resolve, reject) {
                employeeService
                    .createNewClient(registerRequest)
                        .success(function () {
                            resolve();
                        }).error(function (response) {
                            console.error("Cannot register client");
                            reject();
                        });
            });
        };

        $scope.addContract = function () {
            $scope.contractPromise = createContractPromise($scope.newContract);
            $scope.contractPromise.then(function () {
                console.log("Contract successful created");
            });
        };

        var createContractPromise = function (contractRequest) {
            return $q(function (resolve, reject) {
                employeeService
                    .addContract(contractRequest)
                        .success(function () {
                            resolve();
                        }).error(function (response) {
                            console.error("Cannot create contract");
                            reject();
                        });
            });
        };
       
        $scope.closeContract = function (contractId) {
            $scope.closeContractPromise = createCloseContractPromise(contractId);
            $scope.closeContractPromise.then(function () {
                $scope.getContracts($scope.client.clientId)
            });
        };

        var createCloseContractPromise = function (contractId) {
            return $q(function (resolve, reject) {
                employeeService.closeContract(contractId)
                .success(function (data) {
                    resolve(data);
                })
                .error(function (response) {
                    console.error("close contract wrong!");
                    console.error(response);
                    reject();
                });
            });
        };

        $scope.getContracts = function (clientId) {
                employeeService.getContracts(clientId).success(function (data) {
                $scope.contracts = data;
                console.log(data);
                if ($scope.contracts[0] != null) {
                    $scope.showContractDiv = true;
                    $scope.notFoundContractDiv = false;
                }
                else {
                    $scope.showContractDiv = false;
                    $scope.notFoundContractDiv = true;
                }
            }).error(function (error) {
                console.error("show contracts wrong!");
            });
        }

            $scope.clearForm = function () {
            $scope.newClient.Name = '',
            $scope.newClient.Surname = '',
            $scope.newClient.Email= '',
            $scope.newClient.PhoneNumber = '',
            $scope.newClient.ClientId = '',
            $scope.addClientDiv = false;
            $scope.updateClientDiv = false;
            };

            $scope.showNewContractForm = function () {
                $scope.showContractForm = true
            };

            $scope.clearNewContractForm = function () {
                $scope.newContract.rate = '',
                $scope.newContract.dateEnd = '',
                $scope.newContract.pawnItemName = '',
                $scope.newContract.pawnItemDescription = '',
                $scope.newContract.buyPrice = '',
                $scope.newContract.sellPrice
               
                $scope.showContractForm = false;
            };

            $scope.saveNewContractInfo = function () {

                var newContractTemp = $scope.newContract;

                var contractRequest = {
                    ClientId: $scope.client.clientId,
                    EmployeeId: 1,//поменять после авторизации работника
                    Percent: newContractTemp.rate,
                    DateEnd: newContractTemp.dateEnd,
                    Name: newContractTemp.pawnItemName,
                    Description: newContractTemp.pawnItemDescription,
                    BuyPrice: newContractTemp.buyPrice,
                    SellPrice: newContractTemp.sellPrice
                };

                $scope.newContract = null;

                $scope.createContractPromise = createCreateContractPromise(contractRequest);
                $scope.createContractPromise.then(function () {
                    $scope.showContractForm = false;
                    $scope.getContracts($scope.client.clientId);
                });
            };

            var createCreateContractPromise = function (contractRequest) {
                return $q(function (resolve, reject) {
                    employeeService.createContract(contractRequest)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("create contract error!");
                        console.error(response);
                        reject();
                    });
                });
            };

            $scope.editForm = function (data) {
                $scope.newClient = {
                    ClientId: data.clientId,
                    Name: data.name,
                    Surname: data.surname,
                    Email: data.email,
                    PhoneNumber: data.phoneNumber
                }
                $scope.updateClientDiv = true;
                console.log(data);
            };

        $scope.updateClient = function () {
            var currentClient = $scope.newClient;
            $scope.newClient = '';
            $scope.updateClientPromise = createUpdateClientPromise(currentClient);
            $scope.updateClientPromise.then(function () {
                $scope.updateClientDiv = false;
            });
        };

        var createUpdateClientPromise = function (currentClient) {
            return $q(function (resolve, reject) {
                employeeService.updateClient(currentClient)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("update client wrong!!!");
                        console.error(response);
                        reject();
                    });
            });
        }

        $scope.saveClient = function () {

            var clientTemp = $scope.newClient;

            var clientRequest = {
                Name: clientTemp.Name,
                Surname: clientTemp.Surname,
                Email: clientTemp.Email,
                PhoneNumber: clientTemp.PhoneNumber                    
            };

            $scope.newClient = null;

            $scope.createClientPromise = createCreateClientPromise(clientRequest);
            $scope.createClientPromise.then(function () {                  
                $scope.addClientDiv = false;
            });
        };

        var createCreateClientPromise = function (clientRequest) {
            return $q(function (resolve, reject) {
                employeeService.createClient(clientRequest)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("create client error!");
                        console.error(response);
                        reject();
                    });
            });           
        };

        $scope.getClient = function (email) {
           
            employeeService.getClientByEmail(email)
                .success(function (data) {
                    $scope.client = data;
                    console.log(data);
                    if (data != null) {
                        $scope.findClientDiv = true;
                        $scope.notFoundDiv = false;
                        $scope.getContracts($scope.client.clientId);
                    }
                    else {
                        $scope.notFoundDiv = true;
                        $scope.findClientDiv = false;
                        $scope.notFoundContractDiv = false;
                        $scope.showContractDiv = false;
                    }
                })
                .error(function (error) { });
        };
    }
})();