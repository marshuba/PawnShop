﻿(function () {
    "use strict";

    angular
       .module('PawnApp')
       .controller("newsController", newsController);
    newsController.$inject = ["$scope", "newsService"];

    function newsController($scope, newsService) {
        $scope.news = [];

        newsService.getNews().success(function (data) {
            $scope.news = data;
            console.log(data);
        }).error(function (error) {});
    }
})();