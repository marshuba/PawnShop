﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .controller("registerClientController", registerClientController);

    registerClientController.$inject =
        ['$q', '$scope', 'registerClientService'];

    function registerClientController($q, $scope, registerClientService) {

        $scope.createClientData = function (client) {

            if (client.Password != client.ConfirmPassword) {
                alert('Incorrect passwords');
            }

            var registerRequest = {
                Email: client.Email,
                Password: client.Password,
                ConfirmPassword: client.ConfirmPassword,
                Name: client.Name,
                Surname: client.Surname,
                PhoneNumber: client.PhoneNumber,
            };

            $scope.clientPromise = createClientPromise(registerRequest);
            $scope.clientPromise.then(function () {
                console.log("Client successful created");
                $scope.client = null;
            });
        };

        var createClientPromise = function (registerRequest) {
            return $q(function (resolve, reject) {
                registerClientService
                    .createClient(registerRequest)
                        .success(function () {
                            resolve();
                        }).error(function (response) {
                            console.error("Cannot register client");
                            reject();
                   });
            });
        };
    }
})();