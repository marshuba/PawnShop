﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .controller("clientController", clientController);

    clientController.$inject = ['$q', '$scope', 'clientService', 'loginService'];

    function clientController($q, $scope, clientService, loginService) {

        $scope.setClientData = function () {

            $scope.data = loginService.getLoggedUserName();
            $q.all([$scope.data]).then(function (object) {
                clientService.getClientInfo(object[0].data)
                .success(function (info) {
                    $scope.clientInfo = info;
                }).error(function () {
                    console.error("Error getting client info");
                });
            });
        };
    }
})();