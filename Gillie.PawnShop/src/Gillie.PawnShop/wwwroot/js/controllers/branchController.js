﻿(function () {
    "use strict";
    angular
       .module('PawnApp')
       .controller("branchController", branchController);
    branchController.$inject = ["$scope", "branchService"];
    function branchController($scope, branchService) {
        $scope.branches = [];
        branchService.getBranches().success(function (data) {
            $scope.branches = data;
            console.log(data);
        }).error(function (error) { });
    }
})();
