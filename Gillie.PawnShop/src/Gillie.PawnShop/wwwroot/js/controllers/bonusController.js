﻿(function () {
    "use strict";
    angular
       .module('PawnApp')
       .controller("bonusController", bonusController);
   bonusController.$inject = ["$scope", "bonusService"];

    function bonusController($scope, bonusService) {
        $scope.bonus = [];
        bonusService.getBonus().success(function (data) {
            $scope.bonus = data;
            console.log(data);
        }).error(function (error) {});
    }
})();
