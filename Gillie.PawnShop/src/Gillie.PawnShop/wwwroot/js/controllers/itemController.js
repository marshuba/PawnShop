﻿(function () {
    "use strict";
    angular
       .module('PawnApp')
       .controller("itemController", itemController);
    itemController.$inject = ['$q', '$scope', 'itemService', '$http'];
    function itemController($q, $scope, itemService, $http) {

        $scope.items = [];

        $scope.getItems = function () {
            itemService.getItems().success(function (data) {
                $scope.items = data;
                console.log(data);                
            }).error(function (error) {
                console.error("show items wrong!");
            });
        }
    }
})();