﻿(function () {
    "use strict";
    angular
       .module('PawnApp')
       .controller("reviewController", reviewController);
    reviewController.$inject = ['$q', '$scope', 'reviewService', '$http'];
    function reviewController($q, $scope, reviewService, $http) {

        $scope.reviews = [];
        $scope.description = '';
        $scope.clientId = '';

        $scope.clearForm = function () {
            $scope.description = '';
            $scope.clientId = '';
            $scope.addReviewDiv = false;
        };

        $scope.saveReviewInfo = function () {
            var description = $scope.description;
            var clientId = $scope.clientId;
             var reviewRequest = {
                 ClientId: clientId,
                 Description: description
             };
             $scope.description = '';
             $scope.clientId = '';

             $scope.createReviewPromise = createCreateReviewPromise(reviewRequest);
             $scope.createReviewPromise.then(function () {
                 $scope.getPublishedReviews();
                 $scope.addReviewDiv = false;
            });
        };

        var createCreateReviewPromise = function (reviewRequest) {
            return $q(function (resolve, reject) {
                reviewService.createReview(reviewRequest)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("create review error!");
                        console.error(response);
                        reject();
                    });
            });
        };

        $scope.getPublishedReviews = function () {
            reviewService.getPublishedReviews().success(function (data) {
                $scope.reviews = data;
                console.log(data);                
            }).error(function (error) {
                console.error("show reviews wrong!");
            });
        }

        $scope.getAdminReviews = function () {
           
            reviewService.getAdminReviews().success(function (data) {
                $scope.reviews = data;
                console.log(data);
            }).error(function (error) {
                console.error("show reviews wrong!");
            });
        }

        $scope.deleteReview = function (index) {
            $scope.deleteReviewPromise = createDeleteReviewPromise(index);
            $scope.deleteReviewPromise.then(function () {
                $scope.getAdminReviews();
            });
        };

        var createDeleteReviewPromise = function (index) {
            return $q(function (resolve, reject) {
                reviewService.deleteReview(index)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("delete review wrong!");
                        console.error(response);
                        reject();
                    });
            });
        };

        $scope.publishReview = function (index) {
            $scope.updateReviewPromise = createUpdateReviewPromise(index);
            $scope.updateReviewPromise.then(function () {
                $scope.getAdminReviews();
            });
        };

        var createUpdateReviewPromise = function (index) {
            return $q(function (resolve, reject) {
                reviewService.updateReview(index)
                    .success(function (data) {
                        resolve(data);
                    })
                    .error(function (response) {
                        console.error("update review wrong!");
                        console.error(response);
                        reject();
                    });
            });
        };
    }
})();