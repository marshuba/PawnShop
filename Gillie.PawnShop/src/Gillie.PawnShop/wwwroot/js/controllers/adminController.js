﻿(function () {
    'use strict';

    angular
     .module('PawnApp')
    .controller('adminController', adminController);

    adminController.$inject = ['$q', '$scope', 'adminService','$http'];

    function adminController($q, $scope, adminService) {
        $scope.records = [];
        $scope.feedbacks = [];
        $scope.historyRecord = {
            HistoryRecordId: '',
            HistoryRecordTypeId: '',
            CompanyId: '',
            HistoryRecordDate: '',
            Name: '',
            Description: ''
        };
		
		$scope.clientInfo = {
            input: false,
            email: ""
        };

        $scope.employeeInfo = {
            input: false,
            email: "",
            firstDate: "",
            lastDate: ""
        };

        $scope.employees = [];

        $scope.getEmployees = function () {
            adminService.getEmployees()
                .success(function (data) {
                    $scope.employees = data;
                    console.log("successful");
                }).error(function () {
                    console.error("error");
                });
        };

        $scope.getRecords = function() {
            adminService.getRecord()
                .success(function(data) {
                    $scope.records = data;
                    console.log(data);
                })
                .error(function(error) {});
        };

        $scope.clearForm = function () {
            $scope.historyRecord.HistoryRecordId = '',
            $scope.historyRecord.HistoryRecordTypeId = '',
            $scope.historyRecord.CompanyId = '',
            $scope.historyRecord.HistoryRecordDate = '',
            $scope.historyRecord.Name = '',
            $scope.historyRecord.Description = '',
            $scope.addNewDiv = false;
            $scope.updateDiv = false;
        };

        $scope.saveRecordInfo = function () {
           if ($scope.historyRecord.HistoryRecordTypeId != '' &&
                $scope.historyRecord.CompanyId != '' &&
                $scope.historyRecord.HistoryRecordDate != '' &&
                $scope.historyRecord.Name != '' &&
                $scope.historyRecord.Description != '') {
               var record = $scope.historyRecord;

               var recordRequest = {
                   Name: record.Name,
                   Description: record.Description,
                   HistoryRecordDate: record.HistoryRecordDate,
                   CompanyId: record.CompanyId,
                   HistoryRecordTypeId:record.HistoryRecordTypeId
               };

               $scope.historyRecord = null;

               $scope.createRecordPromise = createCreateRecordPromise(recordRequest);
               $scope.createRecordPromise.then(function() {
                   $scope.getRecords();
                   $scope.addNewDiv = false;
               });
           }
            else {
                alert('Please enter all the values!!');
            }
        };

        var createCreateRecordPromise = function(recordRequest) {
            return $q(function (resolve, reject) {
                adminService.createRecord(recordRequest)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("create record error!");
                        console.error(response);
                        reject();
                    });
            });
        };

        $scope.deleteRecord = function (index) {
            $scope.deleteRecordPromise = createDeleteRecordPromise(index);
            $scope.deleteRecordPromise.then(function() {
                $scope.getRecords();
            });
        };

        var createDeleteRecordPromise = function(index) {
            return $q(function(resolve, reject) {
                adminService.deleteRecord(index)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("deleterecord wrong!");
                        console.error(response);
                        reject();
                    });
            });
        };

        $scope.editRecordInfo = function(data) {
            $scope.historyRecord = {
                HistoryRecordId: data.historyRecordId,
                HistoryRecordTypeId: data.historyRecordTypeId,
                CompanyId: data.companyId,
                HistoryRecordDate: data.historyRecordDate,
                Name: data.name,
                Description: data.description
            };

            $scope.updateDiv = true;
            console.log(data);
        };

        $scope.updateRecordInfo = function () {
            var record = $scope.historyRecord;
            $scope.historyRecord = '';
            $scope.updateRecordPromise = createUpdateRecordPromise(record);
            $scope.updateRecordPromise.then(function() {
                $scope.getRecords();
                $scope.updateDiv = false;
            });
        };

        var createUpdateRecordPromise = function(record) {
            return $q(function(resolve, reject) {
                adminService.updateRecord(record)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("update record wrong!");
                        console.error(response);
                        reject();
                    });
            });
        };

        $scope.createClientsReport = function () {

            adminService.createClientsReport()
                .success(function () {
                    alert("Success create report");
                })
                .error(function () {
                    alert("Error create report");
                });

            $scope.clearClientInfo();
        };

        $scope.createClientContractsReport = function () {
            adminService.createClientContractsReport($scope.clientInfo.email)
                .success(function () {
                    alert("Success create report");
                })
                .error(function () {
                    alert("Error create report");
                });

            $scope.clearClientInfo();
        };

        $scope.createEmployeesReport = function () {

            adminService.createEmployeesReport()
                .success(function () {
                    alert("Success create report");
                })
                .error(function () {
                    alert("Error create report");
                });

            $scope.clearEmployeeInfo();
        };

        $scope.createEmployeeProfitReport = function () {
            adminService.createEmployeeProfitReport($scope.employeeInfo.email,
                $scope.employeeInfo.firstDate, $scope.employeeInfo.lastDate)
                .success(function () {
                    alert("Success create report");
                })
                .error(function () {
                    alert("Error create report");
                });
            $scope.clearEmployeeInfo();
        };

        $scope.createPawnItemOnStockReport = function () {

            adminService.createPawnItemOnStockReport()
                .success(function () {
                    alert("Success create report");
                })
                .error(function () {
                    alert("Error create report");
                });
        };

        $scope.clearClientInfo = function () {
            $scope.clientInfo.input = false;
            $scope.clientInfo.email = "";
        };

        $scope.clearEmployeeInfo = function () {
            $scope.employeeInfo.input = false;
            $scope.employeeInfo.email = "";
            $scope.employeeInfo.firstDate = null;
            $scope.employeeInfo.lastDate = null;
        };
    };
})();