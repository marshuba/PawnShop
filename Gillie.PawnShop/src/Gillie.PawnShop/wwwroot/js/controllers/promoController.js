﻿(function () {
    "use strict";

    angular
       .module('PawnApp')
       .controller("promoController", promoController);
    promoController.$inject = ["$scope", "promoService"];

    function promoController($scope, promoService) {

        $scope.promo = [];
        $scope.services = [];

        $scope.getServices = function () {
            promoService.getAllServices()
				.success(function (data) {
		                $scope.services = data;
		                console.log(data);
		            }).error(function (error) {
		                console.error("show services wrong!!");
		            });
        };

        $scope.getPromo = function () {

            promoService.getAllPromo().success(function (data) {
                $scope.promo = data;
                console.log(data);
            }).error(function (error) {
                console.error("show promo wrong!!");
            });
        };
    }
})();