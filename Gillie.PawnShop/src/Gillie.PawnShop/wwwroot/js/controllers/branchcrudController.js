﻿(function () {
    'use strict';
    angular
        .module('PawnApp')
        .controller('branchcrudController', branchcrudController);

    branchcrudController.$inject = ['$q', '$scope', 'branchcrudService', '$http'];

    function branchcrudController($q, $scope, branchcrudService) {
        $scope.getBranches = function () {
            branchcrudService.getBranch()
                .success(function (data) {
                    $scope.branches = data;
                    console.log(data);
                })
                .error(function(error) {
                    console.error("show branches wrong!");
                });
        }

        $scope.branches = [];
        $scope.branch = {
            BranchId: '',
            CompanyId: '',
            Name: '',
            PhoneNumber: '',
            BranchType: ''
        };
          
        $scope.clearForm = function () {
                $scope.branch.BranchId = '',
                $scope.branch.CompanyId = '',
                $scope.branch.Name = '',
                $scope.branch.PhoneNumber = '',
                $scope.branch.BranchType = '',
                $scope.addBranchDiv = false;
                $scope.updateBranchDiv = false;
        };
        
        $scope.saveBranchInfo = function() {
            var branchTemp = $scope.branch;
            var branchRequest = {
                CompanyId: branchTemp.CompanyId,
                Name: branchTemp.Name,
                PhoneNumber: branchTemp.PhoneNumber,
                BranchType: branchTemp.BranchType
            };
            $scope.branch = null;
            $scope.createBranchPromise = createCreateBranchPromise(branchRequest);
            $scope.createBranchPromise.then(function() {
                $scope.getBranches();
                $scope.addBranchDiv = false;
            });
        };

        var createCreateBranchPromise = function(branchRequest) {
            return $q(function(resolve, reject) {
                branchcrudService.createBranch(branchRequest)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("create branch error!");
                        console.error(response);
                        reject();
                    });
            });
        };
       
        $scope.deleteBranch = function(branchIndex) {
            $scope.deleteBranchPromise = createDeleteBranchPromise(branchIndex);
            $scope.deleteBranchPromise.then(function() {
                $scope.getBranches();
            });
        };
        var createDeleteBranchPromise = function(branchIndex) {
            return $q(function(resolve, reject) {
                branchcrudService.deleteBranch(branchIndex)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("delete branch wrong");
                        console.error(response);
                        reject();
                    });
            });
        };
     
        $scope.editBranchInfo = function(data) {
            $scope.branch = {
                BranchId: data.branchId,
                CompanyId: data.companyId,
                Name: data.name,
                PhoneNumber:data.phoneNumber,
                BranchType: data.branchType
            }
            $scope.updateBranchDiv = true;
            console.log(data);
        }
       
        $scope.updateBranchInfo = function() {
            var branchForUpdate = $scope.branch;
            $scope.branch = '';
            $scope.updateBranchPromise = createUpdateBranchPromise(branchForUpdate);
            $scope.updateBranchPromise.then(function() {
                $scope.getBranches();
                $scope.updateBranchDiv = false;
            });
        };
        var createUpdateBranchPromise = function (branchForUpdate) {
            return $q(function(resolve, reject) {
                branchcrudService.updateBranch(branchForUpdate)
                    .success(function(data) {
                        resolve(data);
                    })
                    .error(function(response) {
                        console.error("update branch wrong!!!");
                        console.error(response);
                        reject();
                    });
            });
        }
    }
})();