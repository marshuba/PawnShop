﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .controller("loginController", loginController);

    loginController.$inject = ['$q', '$scope', 'loginService'];

    function loginController($q, $scope, loginService) {

        $scope.loginUser = function (loginModel) {

            var request = {
                Email: loginModel.Email,
                Password: loginModel.Password,
                RememberMe: loginModel.RememberMe
            };

            $scope.loginPromise = createLoginPromise(request);
            $scope.loginPromise.then(function () {
                $scope.loginModel = null;
            });
        };

        var createLoginPromise = function (request) {
            return $q(function (resolve, reject) {
                loginService.LoginStart(request)
                    .success(function (data) {
                        console.log("Login successful");
                        loginService.redirect(data);
                        resolve();
                    }).error(function (data) {
                        console.error("Login failed");
                        loginService.redirect(data);
                        reject();
                    });
            });
        };

        $scope.logOut = function () {
            loginService.logOut()
                .success(function (data) {
                    loginService.redirect(data);
                }).error(function (error) {
                    console.error(error);
                });
        };
    }
})();