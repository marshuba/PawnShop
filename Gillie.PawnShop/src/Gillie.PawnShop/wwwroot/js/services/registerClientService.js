﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("registerClientService", registerClientService);

    registerClientService.$inject = ["$http"];

    function registerClientService($http) {

        var url = "/api/Register";

        this.createClient = function (client) {
            var newUrl = url + "/CreateClient";
            return $http({
                method: "POST",
                url: newUrl,
                data: client,
                headers: { "Content-Type": "application/json" }
            });
        };
    }
})();