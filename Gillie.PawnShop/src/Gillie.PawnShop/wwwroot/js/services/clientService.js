﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("clientService", clientService);

    clientService.$inject = ["$http"];

    function clientService($http) {

        var url = "/api/Client";
        var clientUrl = "/api/Client/GetClientInfo?email=";

        this.getClientInfo = function (email) {
            return $http.get(clientUrl + email);
        };
    }
})();