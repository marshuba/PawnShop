﻿(function () {
    "use strict";

    angular
        .module("PawnApp").service("employeeService", employeeService);

    employeeService.$inject = ["$http"];

    function employeeService($http) {
        var url = "/api/Employee";

        this.createNewClient = function (client) {
            return $http({
                method: "POST",
                url: "/api/Register/CreateClient",
                data: client,
                headers: { "Content-Type": "application/json" }
            });
        };

        this.addContract = function (data) {
            return $http({
                method: 'POST',
                url: url + "/AddContract",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.getClientByEmail = function (data) {
            return $http({
                method: 'POST',
                url: url + "/GetClient",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.createClient = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CreateClient",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.createContract = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CreateContract",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.updateClient = function (data) {
            return $http({
                method: 'POST',
                url: url + "/UpdateClient",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.getContracts = function (data) {
            return $http({
                method: 'POST',
                url: url + "/GetContracts",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.closeContract = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CloseContract",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };
    }
})();