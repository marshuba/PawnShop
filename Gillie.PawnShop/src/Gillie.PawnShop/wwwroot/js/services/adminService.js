﻿(function () {
    'use strict';

    angular
        .module("PawnApp")
        .service('adminService', adminService);

    adminService.$inject = ['$http'];

    function adminService($http) {
        var url = "/api/Admin";

        this.updateRecord = function (data) {
            return $http({
                method: 'POST',
                url: url + "/UpdateRecord",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.getEmployees = function () {
            return $http.get(url + "/GetAllEmployees");
        }

        this.getRecord = function () {
            return $http.get(url + "/GetAllRecords");
        }

        this.createRecord = function(data) {
            return $http({
                method: 'POST',
                url: url + "/CreateRecord",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.deleteRecord = function(data) {
            return $http({
                method: 'POST',
                url: url + "/DeleteRecord",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };
		
		this.createClientsReport = function () {
            return $http({
                method: 'POST',
                url: url + "/CreateClientsReport"
            });
        };

        this.createClientContractsReport = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CreateClientContractsReport",
                data: '"' + data + '"',
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.createEmployeesReport = function () {
            return $http({
                method: 'POST',
                url: url + "/CreateEmployeesReport"
            });
        };

        this.createEmployeeProfitReport = function (email, firstDate, lastDate) {
            return $http({
                method: 'POST',
                url: url + "/CreateEmployeeProfitReport",
                data: { email: email, firstDate: firstDate.toISOString(), lastDate: lastDate.toISOString() },
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.createPawnItemOnStockReport = function () {
            return $http({
                method: 'POST',
                url: url + "/CreatePawnItemsOnStockReport"
            });
        };
    }
})();