﻿(function () {
    "use strict";

    angular
        .module("PawnApp").service("itemService", itemService);

    itemService.$inject = ["$http"];

    function itemService($http) {
        var url = "/api/Employee";

        this.getItems = function () {
            return $http.get(url + "/GetItems");
        }
    }
})();