﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("branchService", branchService);

    branchService.$inject = ["$http"];

    function branchService($http) {
        function getBranches() {
            return $http.get("/api/branch");
        }
        var serv = {
            getBranches: getBranches
        };
        return serv;
    }
})();