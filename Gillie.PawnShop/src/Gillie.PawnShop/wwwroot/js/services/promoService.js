﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("promoService", promoService);

    promoService.$inject = ["$http"];

    function promoService($http) {
       
        this.getAllServices = function () {
             return $http.get("/api/services");
        };

        this.getAllPromo = function () {
            return $http.get("/api/promo");
        };       
    }
})();