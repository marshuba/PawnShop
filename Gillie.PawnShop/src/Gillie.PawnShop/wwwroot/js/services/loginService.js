﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("loginService", loginService);

    loginService.$inject = ['$http'];

    function loginService($http) {

        var url = "/api/Login";

        this.LoginStart = function (request) {
            return $http({
                method: "POST",
                url: url + "/LoginUser",
                data: request,
                headers: { "Content-Type": "application/json" }
            });
        };

        this.logOut = function () {
            return $http({
                method: "POST",
                url: url + "/LogOut"
            });
        };

        this.getLoggedUserName = function () {
            return $http.get(url + "/GetLoggedUserName");
        };

        this.redirect = function (url) {
            window.location.replace(url);
        };
    };

})();