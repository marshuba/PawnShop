﻿(function () {
    "use strict";

    angular
        .module("PawnApp")
        .service("newsService", newsService);

    newsService.$inject = ["$http"];

    function newsService($http) {
        function getNews() {
            return $http.get("/api/news");
        }
        var serv = {
            getNews: getNews
        };
        return serv;
    }
})();