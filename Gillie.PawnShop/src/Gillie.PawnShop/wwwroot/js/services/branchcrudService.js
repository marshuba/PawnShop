﻿(function () {
    'use strict';

    angular
        .module("PawnApp")
        .service('branchcrudService', branchcrudService);

    branchcrudService.$inject = ['$http'];

    function branchcrudService($http) {
        var url = "/api/Admin";

        this.updateBranch = function (data) {
            return $http({
                method: 'POST',
                url: url + "/UpdateBranch",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.getBranch = function () {
            return $http.get(url + "/GetAllBranches");
        }

        this.createBranch = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CreateBranch",
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.deleteBranch = function (data) {
            return $http({
                method: 'POST',
                url: url + "/DeleteBranch",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };
    }
})();