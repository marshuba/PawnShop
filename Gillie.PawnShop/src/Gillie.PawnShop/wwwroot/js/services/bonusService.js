﻿(function () {
    "use strict";

    angular
        .module("PawnApp").service("bonusService", bonusService);

    bonusService.$inject = ["$http"];

    function bonusService($http) {
        function getBonus() {
            return $http.get("/api/bonus");
        }
        var serv = {
            getBonus: getBonus
        };
        return serv;
    }
})();