﻿(function () {
    'use strict';

    angular
        .module("PawnApp")
        .service('reviewService', reviewService);

    reviewService.$inject = ['$http'];

    function reviewService($http) {
        var url = "/api/Review";

        this.getPublishedReviews = function () {
            return $http.get(url + "/GetPublishedReviews");
        };

        this.getAdminReviews = function () {
            return $http.get(url + "/GetAllReviews");
        };

        this.deleteReview = function (data) {
            return $http({
                method: 'POST',
                url: url + "/DeleteReview",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.updateReview = function (data) {
            return $http({
                method: 'POST',
                url: url + "/UpdateCompanyReview",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };

        this.createReview = function (data) {
            return $http({
                method: 'POST',
                url: url + "/CreateCompanyReview",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            });
        };
    }
})();