﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;
using Gillie.PawnShop.Services.Interfaces;
using Gillie.PawnShop.Request;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("/api/[controller]/[action]")]
    public class RegisterController : ApiController
    {
        private readonly IRegisterService registerService;

        public RegisterController(IRegisterService registerService)
        {
            this.registerService = registerService;
        }

        [HttpPost]
        public async Task CreateClient([FromBody] RegisterRequest request)
        {
            registerService.RoleType = "Client";
            await registerService.RegisterUserAsync(request);
        }

        [HttpPost]
        public async Task CreateEmployee([FromBody] RegisterRequest request)
        {
            registerService.RoleType = "Employee";
            await registerService.RegisterUserAsync(request);
        }
    }
}
