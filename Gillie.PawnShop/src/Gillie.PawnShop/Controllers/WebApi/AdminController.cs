﻿using System.Linq;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.BranchRepositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces;
using Gillie.PawnShop.Request;
using Gillie.PawnShop.Services.ReportService.Interfaces;
using Gillie.PawnShop.Services.EmployeeService.Interfaces;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class AdminController : Controller
    {
        private readonly IHistoryRecordsReadRepository historyRepository;
        private readonly IHistoryRecordsWriteRepository historyRepositoryWrite;
        private readonly IBranchReadRepository branchRepositoryRead;
        private readonly IBranchWriteRepository branchRepositoryWrite;

        private readonly IEmployeeService employeeService;
        private readonly IReportService reportService;

        public AdminController(IHistoryRecordsReadRepository historyRepository,
            IHistoryRecordsWriteRepository historyRepositoryWrite,
            IBranchReadRepository branchRepositoryRead,
            IBranchWriteRepository branchRepositoryWrite,
            IReportService reportService, 
            IEmployeeService employeeService)
        {
            this.historyRepository = historyRepository;
            this.historyRepositoryWrite = historyRepositoryWrite;
            this.branchRepositoryRead = branchRepositoryRead;
            this.branchRepositoryWrite = branchRepositoryWrite;
            this.reportService = reportService;
            this.employeeService = employeeService;
        }

        [HttpGet]
        public async Task<HistoryRecord[]> GetAllRecords()
        {
            return await historyRepository.GetAllHistoryRecordsAsync();
        }

        [HttpGet]
        public async Task<Branch[]> GetAllBranches()
        {
            return await branchRepositoryRead.GetAllBranchesAsync();
        }

        [HttpGet]
        public async Task<Employee[]> GetAllEmployees()
        {
            return await employeeService.GetEmployees();
        }

        [HttpPost]
        public async Task CreateRecord([FromBody] HistoryRecordRequest request)
        {
            var record = new HistoryRecord()
            {
                CompanyId = request.CompanyId,
                Description = request.Description,
                HistoryRecordDate = request.HistoryRecordDate,
                Name = request.Name,
                HistoryRecordTypeId = request.HistoryRecordTypeId,
            };

            await historyRepositoryWrite.AddAsyncHistoryRecords(record);
        }

        [HttpPost]
        public async Task CreateBranch([FromBody] BranchRequest request)
        {
            var branch = new Branch()
            {
                CompanyId = request.CompanyId,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                BranchType = request.BranchType
            };
            await branchRepositoryWrite.AddAsyncBranch(branch);
        }

        [HttpPost]
        public async Task DeleteRecord([FromBody] long recordId)
        {
            await historyRepositoryWrite.RemoveAsyncHistoryRecords(recordId);
        }

        [HttpPost]
        public async Task DeleteBranch([FromBody] long branchId)
        {
            await branchRepositoryWrite.RemoveAsyncBranch(branchId);
        }

        [HttpPost]
        public async Task UpdateRecord([FromBody] HistoryRecordRequest request)
        {
            var record = new HistoryRecord()
            {
                CompanyId = request.CompanyId,
                Description = request.Description,
                HistoryRecordDate = request.HistoryRecordDate,
                Name = request.Name,
                HistoryRecordTypeId = request.HistoryRecordTypeId,
                HistoryRecordId = request.HistoryRecordId
            };
            await historyRepositoryWrite.UpdateAsyncHistoryRecords(record);
        }

        [HttpPost]
        public async Task UpdateBranch([FromBody] BranchRequest request)
        {
            var branch = new Branch()
            {
                CompanyId = request.CompanyId,
                Name = request.Name,
                BranchId = request.BranchId,
                PhoneNumber = request.PhoneNumber,
                BranchType = request.BranchType
            };
            await branchRepositoryWrite.UpdateAsyncBranch(branch);
        }

        [HttpPost]
        public void CreateClientsReport()
        {
            reportService.PrintClientsReport();
        }

        [HttpPost]
        public void CreateClientContractsReport([FromBody] string email)
        {
            reportService.PrintContractByClientsReport(email);
        }

        [HttpPost]
        public void CreateEmployeesReport()
        {
            reportService.PrintEmployeesReport();
        }

        [HttpPost]
        public void CreateEmployeeProfitReport([FromBody] EmployeeProfitReportRequest employeeReportRequest)
        {
            reportService.PrintEmployeeProfitReport(
                employeeReportRequest.Email, 
                employeeReportRequest.FirstDate.AddDays(1).Date, 
                employeeReportRequest.LastDate.AddDays(1).Date);
        }

        [HttpPost]
        public void CreatePawnItemsOnStockReport()
        {
            reportService.PrintPawnItemsOnStockReport();
        }
    }
}