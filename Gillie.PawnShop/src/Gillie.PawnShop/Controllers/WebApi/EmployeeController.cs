﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Entities.Enums;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;
using Gillie.PawnShop.Repository.ContractRepository.Interfaces;
using Gillie.PawnShop.Repository.PawnItemRepository.Interfaces;
using Gillie.PawnShop.Request;
using Gillie.PawnShop.Responce;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class EmployeeController : Controller
    {
        private readonly IClientReadRepository clientReadRepository;
        private readonly IClientWriteRepository clientWriteRepository;
        private readonly IContractReadRepository contractReadRepository;
        private readonly IContractWriteRepository contractWriteRepository;
        private readonly IPawnItemReadRepository pawnItemReadRepository;
        private readonly IPawnItemWriteRepository pawnItemWriteRepository;

        public EmployeeController(IClientWriteRepository clientWriteRepository,
            IClientReadRepository clientReadRepository,
            IContractReadRepository contractReadRepository,
            IPawnItemReadRepository pawnItemReadRepository,
            IContractWriteRepository contractWriteRepository,
            IPawnItemWriteRepository pawnItemWriteRepository)
        {
            this.clientReadRepository = clientReadRepository;
            this.clientWriteRepository = clientWriteRepository;
            this.contractReadRepository = contractReadRepository;
            this.contractWriteRepository = contractWriteRepository;
            this.pawnItemReadRepository = pawnItemReadRepository;
            this.pawnItemWriteRepository = pawnItemWriteRepository;
        }

        [HttpPost]
        public async Task<Client> GetClient([FromBody] string email)
        {
            return await clientReadRepository.GetClientByEmailAsync(email);
        }

        [HttpPost]
        public Task<Contract[]> GetContracts([FromBody] string clientId)
        {
            int clientIntId = Convert.ToInt32(clientId);
            return contractReadRepository.GetContractsByClientIdAsync(clientIntId);
        }

        [HttpGet]
        public async Task<PawnItem[]> GetItems()
        {
            return await pawnItemReadRepository.GetPawnItemsAsync();
        }

        [HttpPost]
        public async Task CreateClient([FromBody] ClientRequest request)
        {
            var client = new Client()
            {
                Name = request.Name,
                Surname = request.Surname,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber
            };
            await clientWriteRepository.Add(client);
        }

        [HttpPost]
        public async Task CreateContract([FromBody] ContractRequest request)
        {
            var lastId = contractReadRepository.GetContractWithLastId().Result;
            var helpId = lastId + 1;
            var contract = new Contract()
            {
                ClientId = request.ClientId,
                EmployeeId = request.EmployeeId,
                Percents = request.Percent,
                DateStart = DateTime.Now,
                DateEnd = request.DateEnd,
                IsClosed = false,
                ContractId = helpId
            };
            await contractWriteRepository.AddContract(contract);

            var pawnItem = new PawnItem()
            {
                ContractId = helpId,
                Name = request.Name,
                Description = request.Description,
                BuyPrice = request.BuyPrice,
                SellPrice = request.SellPrice,
                PawnItemState = PawnItemState.OnStockBelongClient
            };

            await pawnItemWriteRepository.AddPawnItem(pawnItem);
        }

        [HttpPost]
        public async Task UpdateClient([FromBody] ClientRequest request)
        {
            var client = new Client()
            {
                ClientId = request.ClientId,
                Name = request.Name,
                Surname = request.Surname,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber
            };
            await clientWriteRepository.Update(client);
        }

        [HttpPost]
        public Task CloseContract([FromBody] long contractId)
        {
            return Task.Run(async () =>
            {
                var contract = await contractReadRepository.GetContractByContractIdAsync(contractId);
                contract.IsClosed = true;
                contract.DateEnd = DateTime.Now;
                var item = contract.PawnItem;
                item.PawnItemState = PawnItemState.ReturnedToClient;
                await pawnItemWriteRepository.ChangePawnItemStateOnReturnAsync(item);
                await contractWriteRepository.CloseContractAsync(contract);
            });
        }

        [HttpPost]
        public async Task AddContract(ContractItmRequest request)
        {
            var item = new PawnItem()
            {
                Name = request.ItemName,
                Description = request.Description,
                BuyPrice = request.BuyPrice,
                SellPrice = request.SellPrice,
                PawnItemState = PawnItemState.OnStockBelongClient
            };

            var contract = new Contract()
            {
                DateStart = DateTime.Now.Date,
                DateEnd = request.EndDate,
                IsClosed = false,
                PawnItem = item,
                Percents = request.Percents
            };

            item.Contract = contract;

            await contractWriteRepository.AddContract(contract);
            await pawnItemWriteRepository.AddPawnItem(item);
        }
    }
}
