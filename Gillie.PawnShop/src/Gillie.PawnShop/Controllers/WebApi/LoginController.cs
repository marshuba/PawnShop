﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Request;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Web.Http;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.UserRoleRepository.Interfaces;
using Gillie.PawnShop.Authorization.AspNet.Identity.Infrastructure.Repository.RoleRepository.Interfaces;
using System.Linq;
using Gillie.PawnShop.Authorization.Entities.Enums;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("/api/[controller]/[action]")]
    public class LoginController : ApiController
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IUserRoleReadRepository userRoleReadRepository;
        private readonly IRoleReadRepository roleReadRepository;

        public LoginController(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IUserRoleReadRepository userRoleReadRepository,
            IRoleReadRepository roleReadRepository)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.userRoleReadRepository = userRoleReadRepository;
            this.roleReadRepository = roleReadRepository;
        }

        [HttpPost]
        public async Task<string> LoginUser([FromBody] LoginRequest model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);

            if (user != null)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    var usersAndRoles = await userRoleReadRepository.GetUserAndRolesAsync();
                    var roles = await roleReadRepository.GetRolesAsync();

                    var roleId = (usersAndRoles.Where(userRole => userRole.UserId == user.Id).FirstOrDefault()).RoleId;
                    var type = (AccountType)roleId;

                    return type.ToString();
                }
            }

            return "Error";
        }

        [HttpPost]
        public async Task<string> LogOut()
        {
            await signInManager.SignOutAsync();
            return "Home";
        }

        [HttpGet]
        public async Task<string> GetLoggedUserName()
        {
            return await Task.Run(() => { return User.Identity.Name; });
        }
    }
}
