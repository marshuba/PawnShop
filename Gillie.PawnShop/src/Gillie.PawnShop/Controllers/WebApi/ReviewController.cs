﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces;
using Gillie.PawnShop.Responce;
using Gillie.PawnShop.Request;
using Gillie.PawnShop.Entities;
using System;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class ReviewController : Controller
    {
        private readonly ICompanyReviewReadRepository companyPeviewsReadRepository;
        private readonly ICompanyReviewWriteRepository companyPeviewsWriteRepository;
        private readonly IClientReadRepository clientReadRepository;

        public ReviewController(ICompanyReviewReadRepository companyPeviewsReadRepository,
                                ICompanyReviewWriteRepository companyPeviewsWriteRepository,
                                IClientReadRepository clientReadRepository)
        {
            this.companyPeviewsReadRepository = companyPeviewsReadRepository;
            this.companyPeviewsWriteRepository = companyPeviewsWriteRepository;
            this.clientReadRepository = clientReadRepository;
        }

        [HttpGet]
        public async Task<CompanyReviewResponce[]> GetPublishedReviews()
        {
            return await companyPeviewsReadRepository.GetPublishedCompanyReviewAsync();
        }

        [HttpGet]
        public async Task<CompanyReviewResponce[]> GetAllReviews()
        {
            return await companyPeviewsReadRepository.GetAllCompanyReviewAsync();
        }

        [HttpPost]
        public async Task DeleteReview([FromBody] long reviewId)
        {
            await companyPeviewsWriteRepository.RemoveAsyncCompanyReview(reviewId);
        }

        [HttpPost]
        public  Task UpdateCompanyReview([FromBody] long reviewId)
        {
            return Task.Run(async () =>
            {
                var record = await companyPeviewsReadRepository.GetCompanyReviewByIdAsync(reviewId);
                record.Publish = true;
                await companyPeviewsWriteRepository.UpdateAsyncCompanyReview(record);
            });
        }

        [HttpPost]
        public async Task CreateCompanyReview([FromBody] CompanyReviewRequest request)
        {
            var client = await clientReadRepository.GetClientByEmailAsync(User.Identity.Name);
            var companyReview = new CompanyReview()
            {
                ClientId = client.ClientId,
                Description = request.Description,
                ReviewDate = DateTime.Now,
                Publish = false
            };

            await companyPeviewsWriteRepository.AddAsyncCompanyReview(companyReview);
        }
    }
}
