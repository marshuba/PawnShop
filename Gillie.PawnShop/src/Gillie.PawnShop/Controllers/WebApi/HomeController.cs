﻿using Microsoft.AspNetCore.Mvc;
using Gillie.PawnShop.Repository.BranchRepositories.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces;
using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Controllers.WebApi
{
    public class HomeController : Controller
    {
        private readonly IBranchReadRepository  repository;
        private readonly IHistoryRecordsReadRepository historyRepository;

        public HomeController(IBranchReadRepository readRepository, IHistoryRecordsReadRepository readHistoryRepository)
        {
            repository = readRepository;
            historyRepository = readHistoryRepository;
        }

        [Route("/api/bonus")]
        public async Task<HistoryRecord[]> GetBonus()
        {
            return await historyRepository.GetAllHistoryRecordsAsync(RecordType.Bonus);
        }

        [Route("/api/news")]
        public async Task<HistoryRecord[]> GetNews()
        {
            return await historyRepository.GetAllHistoryRecordsAsync(RecordType.News);
        }

        [Route("/api/branch")]
        public async Task<Branch[]> GetBranches()
        {
            return await repository.GetAllBranchesAsync();
        }

        [Route("/api/promo")]
        public async Task<HistoryRecord[]> GetPromo()
        {
            return await historyRepository.GetAllHistoryRecordsAsync(RecordType.Promo);
        }

        [Route("/api/services")]
        public async Task<HistoryRecord[]> GetServices()
        {
            return await historyRepository.GetAllHistoryRecordsAsync(RecordType.Service);
        }
    }
}
