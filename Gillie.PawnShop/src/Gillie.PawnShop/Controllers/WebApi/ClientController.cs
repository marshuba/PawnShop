﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;
using Gillie.PawnShop.Repository.ContractRepository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Gillie.PawnShop.Controllers.WebApi
{
    [Route("/api/[controller]/[action]")]
    public class ClientController : ApiController
    {
        private readonly IClientReadRepository clientReadRepository;
        private readonly IContractReadRepository contractReadRepository;
        private readonly IClientLocationReadRepository clientLocationReadRepository;

        public ClientController(
            IClientReadRepository clientReadRepository, 
            IContractReadRepository contractReadRepository,
            IClientLocationReadRepository clientLocationReadRepository)
        {
            this.clientReadRepository = clientReadRepository;
            this.contractReadRepository = contractReadRepository;
            this.clientLocationReadRepository = clientLocationReadRepository;
        }

        [HttpGet]
        public async Task<Client> GetClientInfo(string email)
        {
            return await GetClient(email);
        }

        [HttpGet]
        public async Task<ClientLocation> GetClientLocation(string email)
        {
            var client = await GetClient(email);
            return (await clientLocationReadRepository.GetClientLocationsAsync()).
                FirstOrDefault(clientLocation => clientLocation.ClientId == client.ClientId);
        }

        [HttpGet]
        public async Task<Contract[]> GetClientContracts(string email)
        {
            var client = await GetClient(email);
            return (await contractReadRepository.GetContractsAsync())
                .Where(contract => contract.ClientId == client.ClientId).ToArray();
        }

        private async Task<Client> GetClient(string email)
        {
            return await clientReadRepository.GetClientByEmailAsync(email);
        }
    }
}
