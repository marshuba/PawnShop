﻿using Microsoft.AspNetCore.Mvc;

namespace Gillie.PawnShop.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Promotion()
        {
            return View();
        }

        public IActionResult Branches()
        {
            return View();
        }

        public IActionResult Bonus()
        {
            return View();
        }

        public IActionResult Service()
        {
            return View();
        }
    }
}
