﻿using Microsoft.AspNetCore.Mvc;

namespace Gillie.PawnShop.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View("Error");
        }
    }
}
