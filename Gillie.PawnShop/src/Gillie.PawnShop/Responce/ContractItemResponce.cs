﻿using System;

namespace Gillie.PawnShop.Responce
{
    public class ContractItemResponce
    {
        public double Percents { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool IsClosed { get; set; } = false;

        public string Name { get; set; }
        public string Description { get; set; }
        public double BuyPrice { get; set; }
        public double SellPrice { get; set; }
        public bool IsOwnShop { get; set; } = false;
    }
}
