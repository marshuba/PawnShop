﻿using Gillie.PawnShop.Entities;

namespace Gillie.PawnShop.Responce
{
    public class ContractPawnItemResponce
    {
        public Contract Contract { get; set; }
        public PawnItem PawnItem { get; set; }
    }
}