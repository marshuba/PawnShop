﻿using System;

namespace Gillie.PawnShop.Responce
{
    public class CompanyReviewResponce
    {
        public long ClientId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public long CompanyReviewId { get; set; }
        public DateTime ReviewDate { get; set; }
        public string Description { get; set; }
        public bool Publish { get; set; }
    }
}
