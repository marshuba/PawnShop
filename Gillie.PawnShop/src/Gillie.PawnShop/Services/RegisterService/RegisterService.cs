﻿using Gillie.PawnShop.Services.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Request;
using Microsoft.AspNetCore.Identity;
using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;
using Gillie.PawnShop.Repository.EmployeeRepository.Interfaces;

namespace Gillie.PawnShop.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly IClientWriteRepository clientWriteRepository;
        private readonly IEmployeeWriteRepository employeeWriteRepository;
        private readonly UserManager<IdentityUser> userManager;

        public RegisterService(
            IClientWriteRepository clientWriteRepository,
            IEmployeeWriteRepository employeeWriteRepository,
            UserManager<IdentityUser> userManager)
        {
            this.clientWriteRepository = clientWriteRepository;
            this.employeeWriteRepository = employeeWriteRepository;
            this.userManager = userManager;
        }

        public string RoleType { get; set; }

        public async Task RegisterUserAsync(RegisterRequest registerRequest)
        {
            if (RoleType == "Client")
            {
                await RegisterClient(registerRequest);
            }
            else
            {
                await RegisterEmployee(registerRequest);
            }

            await CreateIdentity(registerRequest.Email, registerRequest.Password);
        }

        private Task RegisterClient(RegisterRequest registerRequest)
        {
            return Task.Run(async () =>
            {
                var client = CreateClient(
                    registerRequest.Name,
                    registerRequest.Surname,
                    registerRequest.Email,
                    registerRequest.PhoneNumber);

                await clientWriteRepository.Add(client);
            });
        }

        private Task RegisterEmployee(RegisterRequest registerRequest)
        {
            return Task.Run(async () =>
            {
                var employee = CreateEmployee(
                    registerRequest.Name,
                    registerRequest.Surname,
                    registerRequest.Email,
                    registerRequest.PhoneNumber);

                await employeeWriteRepository.Add(employee);
            });
        }

        private async Task CreateIdentity(string email, string password)
        {
            var identityUser = new IdentityUser()
            {
                UserName = email,
                Email = email
            };

            await userManager.CreateAsync(identityUser, password);
            await userManager.AddToRoleAsync(identityUser, RoleType);
        }

        private Client CreateClient(string name, string surname, string email, string phoneNumber)
        {
            return new Client()
            {
                Name = name,
                Surname = surname,
                Email = email,
                PhoneNumber = phoneNumber
            };
        }

        private Employee CreateEmployee(string name, string surname, string email, string phoneNumber)
        {
            return new Employee()
            {
                Name = name,
                Surname = surname,
                Email = email,
                PhoneNumber = phoneNumber
            };
        }
    }
}
