﻿using Gillie.PawnShop.Request;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.Interfaces
{
    public interface IRegisterService
    {
        Task RegisterUserAsync(RegisterRequest registerRequest);
        string RoleType { get; set; }
    }
}
