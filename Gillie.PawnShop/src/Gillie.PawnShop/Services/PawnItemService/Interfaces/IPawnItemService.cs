﻿using Gillie.PawnShop.Entities;
using System;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.PawnItemService.Interfaces
{
    public interface IPawnItemService
    {
        Task<PawnItem[]> GetPawnItemsAsync();
        Task<PawnItem> GetPawnItemByContract(Contract contract);
        Task<PawnItem[]> GetPawnItemsByContracts(Contract[] contracts);
        Task<PawnItem[]> GetPawnItemsOnStock();

        Task AddPawnItem(PawnItem pawnItem);
        Task UpdatePawnItem(PawnItem pawnItem);
    }
}
