﻿using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.PawnItemRepository.Interfaces;
using System;

namespace Gillie.PawnShop.Services.PawnItemService
{
    public class PawnItemService : IPawnItemService
    {
        private readonly IPawnItemReadRepository pawnItemsReadRepository;
        private readonly IPawnItemWriteRepository pawnItemsWriteRepository;

        public PawnItemService(
            IPawnItemReadRepository pawnItemsReadRepository,
            IPawnItemWriteRepository pawnItemsWriteRepository)
        {
            this.pawnItemsReadRepository = pawnItemsReadRepository;
            this.pawnItemsWriteRepository = pawnItemsWriteRepository;
        }

        public async Task AddPawnItem(PawnItem pawnItem)
        {
            await pawnItemsWriteRepository.AddPawnItem(pawnItem);
        }

        public async Task<PawnItem> GetPawnItemByContract(Contract contract)
        {
            return await pawnItemsReadRepository.GetPawnItemByContractAsync(contract);
        }

        public async Task<PawnItem[]> GetPawnItemsAsync()
        {
            return await pawnItemsReadRepository.GetPawnItemsAsync();
        }

        public async Task<PawnItem[]> GetPawnItemsByContracts(Contract[] contracts)
        {
            return await pawnItemsReadRepository.GetPawnItemsByContractsAsync(contracts);
        }

        public async Task<PawnItem[]> GetPawnItemsOnStock()
        {
            return await pawnItemsReadRepository.GetPawnItemsOnStockAsync();
        }

        public async Task UpdatePawnItem(PawnItem pawnItem)
        {
            await pawnItemsWriteRepository.UpdatePawnItem(pawnItem);
        }
    }
}
