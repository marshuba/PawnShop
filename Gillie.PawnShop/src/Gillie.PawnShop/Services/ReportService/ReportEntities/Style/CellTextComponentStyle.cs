﻿using Gillie.PawnShop.Services.ReportService.ReportParts.Style.Entity;
using iTextSharp.text;

namespace Gillie.PawnShop.Services.ReportService.ReportParts.Style
{
    public class CellTextComponentStyle
    {
        public float ComponentWidth { get; set; }
        public Font TextFont { get; set; }
        public int TextAlignment { get; set; }
        public Padding Padding { get; set; }
        public float XPosition { get; set; }
        public float YPosition { get; set; }
    }
}