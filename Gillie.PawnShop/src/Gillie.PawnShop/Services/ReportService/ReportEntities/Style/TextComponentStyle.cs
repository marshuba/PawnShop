﻿using iTextSharp.text;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style
{
    public class TextComponentStyle
    {
        public Font TextFont { get; set; }
        public int Alignment { get; set; }
        public float SpacingBefore { get; set; }
        public float SpacingAfter { get; set; }
    }
}
