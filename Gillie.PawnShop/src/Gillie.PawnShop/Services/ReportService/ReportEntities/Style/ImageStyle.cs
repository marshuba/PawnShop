﻿namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style
{
    public class ImageStyle
    {
        public float XPosition { get; set; }
        public float YPosition { get; set; }
        public int Alignment { get; set; }
        public float ScalePercent { get; set; }
    }
}
