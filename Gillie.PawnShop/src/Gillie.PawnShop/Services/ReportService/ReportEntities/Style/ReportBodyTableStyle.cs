﻿using iTextSharp.text;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style
{
    public class ReportBodyTableStyle
    {
        public Font HeaderFont { get; set; }
        public Font TableFont { get; set; }
        public BaseColor EvenRowsColor { get; set; }
        public BaseColor OddRowsColor { get; set; }
        public BaseColor HeaderColor { get; set; }
        public float SpacingBefore { get; set; }
        public float SpacingAfter { get; set; }
    }
}
