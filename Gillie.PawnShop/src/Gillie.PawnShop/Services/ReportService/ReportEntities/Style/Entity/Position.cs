﻿namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style.Entity
{
    public class Position
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
