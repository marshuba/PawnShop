﻿namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style.Entity
{
    public class Margin
    {
        public float Left { get; set; }
        public float Right { get; set; }
        public float Top { get; set; }
        public float Bottom { get; set; }
    }
}
