﻿namespace Gillie.PawnShop.Services.ReportService.ReportParts.Style.Entity
{
    public class Padding
    {
        public float Left { get; set; }
        public float Right { get; set; }
        public float Top { get; set; }
        public float Bottom { get; set; }
    }
}