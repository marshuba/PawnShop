﻿using iTextSharp.text;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Style
{
    public class LineStyle
    {
        public BaseColor LineColor { get; set; }
        public float StartX { get; set; }
        public float StartY { get; set; }
        public float EndX { get; set; }
        public float EndY { get; set; }
    }
}
