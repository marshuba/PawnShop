﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.Reports
{
    public abstract class AbstractReport : IReport
    {
        protected Document document;
        protected FileStream stream;
        protected PdfWriter pdfWriter;

        public AbstractReport(string fileName, Rectangle pageSize)
        {
            InitDocument(fileName, pageSize);
        }

        public AbstractReport(string fileName, Rectangle pageSize, PdfPageEventHelper helper)
        {
            InitDocument(fileName, pageSize);
            pdfWriter.PageEvent = helper;
        }

        public void SetMargins(float marginLeft, float marginRight, float marginTop, float marginBottom)
        {
            document.SetMargins(marginLeft, marginRight, marginTop, marginBottom);
        }

        private void InitDocument(string fileName, Rectangle pageSize)
        {
            stream = new FileStream(fileName + ".pdf", FileMode.Create, FileAccess.Write);
            document = new Document(pageSize);
            pdfWriter = PdfWriter.GetInstance(document, stream);
        }

        public abstract void Print();
    }
}
