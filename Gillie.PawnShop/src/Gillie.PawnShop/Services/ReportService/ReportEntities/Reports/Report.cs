﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.Reports;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;

namespace Gillie.PawnShop.Services.ReportService.ReportEntites.Parts.Reports
{
    public class Report : AbstractReport
    {
        private readonly IEnumerable<IReportComponent> bodyParts;

        public Report(
            string fileName, 
            Rectangle pageSize, 
            PdfPageEventHelper helper, 
            IEnumerable<IReportComponent> bodyParts) 
            : base(fileName, pageSize, helper)
        {
            this.bodyParts = bodyParts;
        }

        public override void Print()
        {
            document.Open();

            foreach (var part in bodyParts)
            {
                part.Print(pdfWriter, document);
            }

            document.Close();
        }
    }
}