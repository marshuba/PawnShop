﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Collections.Generic;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities
{
    public class PdfReportEvents : PdfPageEventHelper
    {
        private readonly IEnumerable<IReportComponent> reportParts;

        public PdfReportEvents(IEnumerable<IReportComponent> reportParts)
        {
            this.reportParts = reportParts;
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            foreach (var part in reportParts)
            {
                part.Print(writer, document);
            }
        }
    }
}
