﻿namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces
{
    public interface IReport
    {
        void Print();
    }
}
