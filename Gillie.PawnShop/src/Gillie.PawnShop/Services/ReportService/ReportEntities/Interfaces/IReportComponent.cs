﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces
{
    public interface IReportComponent
    {
        void Print(PdfWriter writer, Document document);
    }
}