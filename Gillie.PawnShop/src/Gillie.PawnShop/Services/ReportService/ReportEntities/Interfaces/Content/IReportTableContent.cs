﻿namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content
{
    public interface IReportTableContent
    {
        string[] GetHeaderContent();
        string[][] GetTableContent();
    }
}