﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.Body
{
    public class ReportBodyTable : IReportComponent
    {
        protected PdfPTable table;
        protected string[] header;
        protected string[][] content;
        protected ReportBodyTableStyle tableStyle;

        public ReportBodyTable(float[] relativeWidths, IReportTableContent content, ReportBodyTableStyle tableStyle)
        {
            table = new PdfPTable(relativeWidths);
            header = content.GetHeaderContent();
            this.content = content.GetTableContent();
            this.tableStyle = tableStyle;
            SetTableParams();
        }

        public virtual void Print(PdfWriter writer, Document document)
        {
            document.Add(new Phrase(" "));
            table.Rows.Add(GetMainRow());

            for (int i = 0; i < content.GetLength(0); i++)
            {
                table.Rows.Add(GetContentRow(content[i], i));
            }

            document.Add(table);
        }

        private PdfPRow GetMainRow()
        {
            var mainCells = GetCells(header, tableStyle.HeaderFont);

            foreach (var cell in mainCells)
            {
                cell.BackgroundColor = tableStyle.HeaderColor;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.MinimumHeight = tableStyle.HeaderFont.Size * 2;
                cell.PaddingTop = tableStyle.HeaderFont.Size / 4;
                cell.BorderWidth = 0;
            }

            return new PdfPRow(mainCells);
        }

        private PdfPRow GetContentRow(string[] content, int index)
        {
            var contentCells = GetCells(content, tableStyle.TableFont);
            var color = index % 2 == 0 ? tableStyle.EvenRowsColor : tableStyle.OddRowsColor;

            foreach (var cell in contentCells)
            {
                cell.BackgroundColor = color;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.PaddingLeft = 5f;
                cell.BorderWidth = 0;
            }

            return new PdfPRow(contentCells);
        }

        private PdfPCell[] GetCells(string[] content, Font contentFont)
        {
            var cells = new PdfPCell[content.Length];

            for (int i = 0; i < header.Length; i++)
            {
                cells[i] = new PdfPCell(new Phrase(content[i], contentFont));
            }

            return cells;
        }

        protected virtual void SetTableParams()
        {
            table.SpacingAfter = tableStyle.SpacingAfter;
            table.SpacingBefore = tableStyle.SpacingBefore;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            table.WidthPercentage = 95;
        }
    }
}