﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General
{
    public class TextComponent : IReportComponent
    {
        private readonly Paragraph paragraph;

        public TextComponent(string text, TextComponentStyle style)
        {
            paragraph = new Paragraph(text, style.TextFont);
            paragraph.Alignment = style.Alignment;
            paragraph.SpacingAfter = style.SpacingAfter;
            paragraph.SpacingBefore = style.SpacingBefore;
        }

        public void Print(PdfWriter writer, Document document)
        {
            document.Add(paragraph);
        }
    }
}
