﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportParts.Style;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General
{
    public class CellTextComponent : IReportComponent
    {
        protected readonly string componentText;
        protected readonly CellTextComponentStyle style;

        public CellTextComponent(string componentText, CellTextComponentStyle style)
        {
            this.componentText = componentText;
            this.style = style;
        }

        public virtual void Print(PdfWriter writer, Document document)
        {
            var component = GetComponent();
            component.WriteSelectedRows(0, -1, style.XPosition, style.YPosition, writer.DirectContent);
        }

        protected virtual PdfPTable GetComponent()
        {
            var table = new PdfPTable(1);
            table.TotalWidth = style.ComponentWidth;

            var cellInfo = new PdfPCell(new Phrase(componentText, style.TextFont));
            SetCellStyle(cellInfo);
            table.AddCell(cellInfo);

            return table;
        }

        protected virtual void SetCellStyle(PdfPCell cell)
        {
            cell.HorizontalAlignment = style.TextAlignment;
            cell.Border = 0;
            SetPadding(cell);
        }

        protected void SetPadding(PdfPCell cell)
        {
            cell.PaddingLeft = style.Padding.Left;
            cell.PaddingRight = style.Padding.Right;
            cell.PaddingTop = style.Padding.Top;
            cell.PaddingBottom = style.Padding.Bottom;
        }
    }
}