﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General
{
    public class LineDrawer : IReportComponent
    {
        private readonly LineStyle lineStyle;

        public LineDrawer(LineStyle lineStyle)
        {
            this.lineStyle = lineStyle;
        }

        public void Print(PdfWriter writer, Document document)
        {
            var context = writer.DirectContent;
            context.SetColorStroke(lineStyle.LineColor);
            context.MoveTo(lineStyle.StartX, lineStyle.StartY);
            context.LineTo(lineStyle.EndX, lineStyle.EndY);
            context.Stroke();
        }
    }
}
