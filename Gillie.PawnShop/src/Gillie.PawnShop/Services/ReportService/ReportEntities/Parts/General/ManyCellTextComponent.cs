﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General
{
    public class ManyCellTextComponent : IReportComponent
    {
        private readonly CellTextComponent[] textComponents;

        public ManyCellTextComponent(CellTextComponent[] textComponents)
        {
            this.textComponents = textComponents;
        }

        public void Print(PdfWriter writer, Document document)
        {
            foreach (var component in textComponents)
            {
                component.Print(writer, document);
            }
        }
    }
}