﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General
{
    public class ImageDrawer : IReportComponent
    {
        private readonly Image image;

        public ImageDrawer(string path, ImageStyle imageStyle)
        {
            image = Image.GetInstance(path);
            image.ScalePercent(imageStyle.ScalePercent);
            image.Alignment = imageStyle.Alignment;
        }

        public void Print(PdfWriter writer, Document document)
        {
            document.Add(image);
        }
    }
}