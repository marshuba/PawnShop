﻿using Gillie.PawnShop.Services.ReportService.Factory.Interfaces;
using Gillie.PawnShop.Services.ReportService.Interfaces;
using System;

namespace Gillie.PawnShop.Services.ReportService
{
    public class ReportService : IReportService
    {
        private readonly IReportFactory reportFactory;

        public ReportService(IReportFactory reportFactory)
        {
            this.reportFactory = reportFactory;
        }

        public void PrintContractByClientsReport(string email)
        {
            reportFactory.GetClientContractsReport(email).Print();
        }

        public void PrintClientsReport()
        {
            reportFactory.GetClientsReport().Print();
        }

        public void PrintEmployeesReport()
        {
            reportFactory.GetEmployeesReport().Print();
        }

        public void PrintEmployeeProfitReport(string email, DateTime firstDate, DateTime lastDate)
        {
            reportFactory.GetEmployeeProfitReport(email, firstDate, lastDate).Print();
        }

        public void PrintPawnItemsOnStockReport()
        {
            reportFactory.GetPawnItemsReport().Print();
        }
    }
}