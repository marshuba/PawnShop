﻿using System;

namespace Gillie.PawnShop.Services.ReportService.Interfaces
{
    public interface IReportService
    {
        void PrintContractByClientsReport(string email);
        void PrintClientsReport();
        void PrintEmployeesReport();
        void PrintEmployeeProfitReport(string email, DateTime firstDate, DateTime lastDate);
        void PrintPawnItemsOnStockReport();
    }
}