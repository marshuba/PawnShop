﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class EmployeeProfitReportBodyBuilder : AbstractReportBodyBuilder
    {
        public EmployeeProfitReportBodyBuilder(IReportComponentBuilder componentBuilder) 
            : base(componentBuilder, "EmployeeProfitReport")
        {
            
        }

        public IReportComponent[] GetReportBodyComponents(
            IReportTableContent content, 
            string headerContent,
            string profitContent)
        {
            return new IReportComponent[]
            {
                componentBuilder.GetSignature(headerContent),
                componentBuilder.GetBody(Key, content),
                componentBuilder.GetProfitComponent(profitContent),
                componentBuilder.GetImageDrawer()
            };
        }
    }
}