﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntites.Parts.Reports;
using iTextSharp.text;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.Reports;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style.Entity;
using System.Collections.Generic;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class ReportBuilder : IReportBuilder
    {
        private readonly IReportComponentBuilder componentBuilder;

        public ReportBuilder(IReportComponentBuilder componentBuilder)
        {
            this.componentBuilder = componentBuilder;
        }

        public IReport BuildReport(string key, IEnumerable<IReportComponent> bodyParts)
        {
            var report = new Report(
                componentBuilder.GetReportName(key),
                PageSize.A4,
                componentBuilder.GetPdfTitles(key),
                bodyParts);

            SetMargins(report, componentBuilder.GetMargins(key));
            return report;
        }

        public IReportComponentBuilder ReportComponentBuilder { get { return componentBuilder; } }

        private void SetMargins(AbstractReport report, Margin margins)
        {
            report.SetMargins(margins.Left, margins.Right, margins.Top, margins.Bottom);
        }
    }
}