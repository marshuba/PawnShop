﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style.Entity;
using iTextSharp.text.pdf;

namespace Gillie.PawnShop.Services.ReportService.Builder.Interfaces
{
    public interface IReportComponentBuilder
    {
        IReportComponent GetBody(string key, IReportTableContent content);
        PdfPageEventHelper GetPdfTitles(string key);
        IReportComponent GetImageDrawer();
        IReportComponent GetSignature(string text);
        IReportComponent GetProfitComponent(string text);
        string GetReportName(string key);
        Margin GetMargins(string key);
    }
}