﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using System.Collections.Generic;

namespace Gillie.PawnShop.Services.ReportService.Builder.Interfaces
{
    public interface IReportBuilder
    {
        IReport BuildReport(string key, IEnumerable<IReportComponent> bodyParts);
        IReportComponentBuilder ReportComponentBuilder { get; }
    }
}