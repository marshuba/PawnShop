﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public abstract class AbstractReportBodyBuilder
    {
        protected readonly IReportComponentBuilder componentBuilder;

        public AbstractReportBodyBuilder(IReportComponentBuilder componentBuilder, string key)
        {
            Key = key;
            this.componentBuilder = componentBuilder;
        }

        public string Key { get; }
    }
}