﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class EmployeesReportBodyBuilder : AbstractReportBodyBuilder
    {
        public EmployeesReportBodyBuilder(IReportComponentBuilder componentBuilder) 
            : base(componentBuilder, "EmployeesReport")
        {
            
        }

        public IReportComponent[] GetReportBodyComponents(IReportTableContent content)
        {
            return new IReportComponent[]
            {
                componentBuilder.GetBody(Key, content),
                componentBuilder.GetImageDrawer()
            };
        }
    }
}