﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using System.Collections.Generic;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style.Entity;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Style;
using Gillie.PawnShop.Configuration;
using iTextSharp.text;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.Body;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General;
using Gillie.PawnShop.Services.ReportService.ReportParts.Style;
using Gillie.PawnShop.Services.ReportService.ReportParts.Style.Entity;
using Gillie.PawnShop.Services.ReportService.Reports.Component.Footer;
using Gillie.PawnShop.Services.ReportService.Reports.Component.General;
using Gillie.PawnShop.Services.ReportService.ReportEntities;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class ReportComponentBuilder : IReportComponentBuilder
    {
        private readonly IPdfConfig pdfConfig;

        public ReportComponentBuilder(IPdfConfig pdfConfig)
        {
            this.pdfConfig = pdfConfig;
        }

        public IReportComponent GetBody(string key, IReportTableContent content)
        {
            var relativeWidths = pdfConfig.ReportConfigs[key].RelativeWidths;
            var bodyTableStyle = GetBodyStyle(key);
            return new ReportBodyTable(relativeWidths, content, bodyTableStyle);
        }

        public IReportComponent GetImageDrawer()
        {
            return new ImageDrawer(
                pdfConfig.ImageConfig.ImagePath, 
                new ImageStyle()
            {
                Alignment = pdfConfig.ImageConfig.Alignment,
                ScalePercent = pdfConfig.ImageConfig.ScalePercent
            });
        }

        public Margin GetMargins(string key)
        {
            return new Margin()
            {
                Bottom = pdfConfig.ReportConfigs[key].MarginConfig.Bottom,
                Top = pdfConfig.ReportConfigs[key].MarginConfig.Top,
                Left = pdfConfig.ReportConfigs[key].MarginConfig.Left,
                Right = pdfConfig.ReportConfigs[key].MarginConfig.Right
            };
        }

        public PdfPageEventHelper GetPdfTitles(string key)
        {
            var reportParts = new List<IReportComponent>()
            {
                GetHeader(pdfConfig.ReportConfigs[key].HeaderText),
                GetFooter(pdfConfig.ReportConfigs[key].FooterText)
            };

            return new PdfReportEvents(reportParts);
        }

        public IReportComponent GetProfitComponent(string text)
        {
            return new TextComponent(
                string.Format(pdfConfig
                    .ProfitComponentConfig
                    .FormatText, 
                    text), 
                    GetProfitComponentStyle());
        }

        public string GetReportName(string key)
        {
            return pdfConfig.ReportConfigs[key].ReportName;
        }

        public IReportComponent GetSignature(string text)
        {
            return new CellTextComponent(text, GetSignatureStyle());
        }

        private ReportBodyTableStyle GetBodyStyle(string key)
        {
            return new ReportBodyTableStyle()
            {
                HeaderFont = FontFactory.GetFont(
                    pdfConfig.BodyConfig.TableHeaderFontConfig.Name,
                    pdfConfig.BodyConfig.TableHeaderFontConfig.Size,
                    pdfConfig.BodyConfig.TableHeaderFontConfig.Style,
                    new BaseColor(
                        pdfConfig.BodyConfig.TableHeaderFontConfig.ColorConfig.Red,
                        pdfConfig.BodyConfig.TableHeaderFontConfig.ColorConfig.Green,
                        pdfConfig.BodyConfig.TableHeaderFontConfig.ColorConfig.Blue)),

                TableFont = FontFactory.GetFont(
                    pdfConfig.BodyConfig.TableFontConfig.Name,
                    pdfConfig.BodyConfig.TableFontConfig.Size,
                    pdfConfig.BodyConfig.TableFontConfig.Style,
                    new BaseColor(
                        pdfConfig.BodyConfig.TableFontConfig.ColorConfig.Red,
                        pdfConfig.BodyConfig.TableFontConfig.ColorConfig.Green,
                        pdfConfig.BodyConfig.TableFontConfig.ColorConfig.Blue)),

                HeaderColor = new BaseColor(
                    pdfConfig.BodyConfig.TableHeaderColorConfig.Red,
                    pdfConfig.BodyConfig.TableHeaderColorConfig.Green,
                    pdfConfig.BodyConfig.TableHeaderColorConfig.Blue),

                EvenRowsColor = new BaseColor(
                    pdfConfig.BodyConfig.EvenRowsColorConfig.Red,
                    pdfConfig.BodyConfig.EvenRowsColorConfig.Green,
                    pdfConfig.BodyConfig.EvenRowsColorConfig.Blue),

                OddRowsColor = new BaseColor(
                    pdfConfig.BodyConfig.OddRowsColorConfig.Red,
                    pdfConfig.BodyConfig.OddRowsColorConfig.Green,
                    pdfConfig.BodyConfig.OddRowsColorConfig.Blue),

                SpacingAfter = pdfConfig.ReportConfigs[key].SpacingAfter,
                SpacingBefore = pdfConfig.ReportConfigs[key].SpacingBefore
            };
        }

        private IReportComponent GetHeader(string text)
        {
            var headerStyle = GetHeaderStyle();
            var dateStyle = GetDateStyle();
            var lineStyle = GetLineStyle(
                pdfConfig.HeaderConfig.LineConfig.StartX,
                pdfConfig.HeaderConfig.LineConfig.StartY,
                pdfConfig.HeaderConfig.LineConfig.EndX,
                pdfConfig.HeaderConfig.LineConfig.EndY);

            var headerText = new CellTextComponent(text, headerStyle);
            var lineDrawer = new LineDrawer(lineStyle);
            var header = new HeaderWithDate(headerText, lineDrawer, dateStyle);

            return header;
        }

        private IReportComponent GetFooter(string text)
        {
            var footerStyle = GetFooterStyle();
            var pageNumberStyle = GetPageNumberStyle();
            var lineStyle = GetLineStyle(
                pdfConfig.FooterConfig.LineConfig.StartX,
                pdfConfig.FooterConfig.LineConfig.StartY,
                pdfConfig.FooterConfig.LineConfig.EndX,
                pdfConfig.FooterConfig.LineConfig.EndY);

            var footerText = new CellTextComponent(text, footerStyle);
            var lineDrawer = new LineDrawer(lineStyle);
            var footer = new FooterWithNumeration(footerText, lineDrawer, pageNumberStyle);

            return footer;
        }

        private CellTextComponentStyle GetFooterStyle()
        {
            return new CellTextComponentStyle()
            {
                ComponentWidth = pdfConfig.FooterConfig.LeftTitleConfig.Width,
                TextAlignment = pdfConfig.FooterConfig.LeftTitleConfig.TextAlignment,

                TextFont = FontFactory.GetFont(
                    pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.Name,
                    pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.Size,
                    pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.Style,
                    new BaseColor(
                        pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.FooterConfig.LeftTitleConfig.FontConfig.ColorConfig.Blue)),

                XPosition = pdfConfig.FooterConfig.LeftTitleConfig.XPosition,
                YPosition = pdfConfig.FooterConfig.LeftTitleConfig.YPosition,

                Padding = new Padding()
                {
                    Bottom = pdfConfig.FooterConfig.LeftTitleConfig.PaddingConfig.Bottom,
                    Left = pdfConfig.FooterConfig.LeftTitleConfig.PaddingConfig.Left,
                    Right = pdfConfig.FooterConfig.LeftTitleConfig.PaddingConfig.Right,
                    Top = pdfConfig.FooterConfig.LeftTitleConfig.PaddingConfig.Top
                }
            };
        }

        private CellTextComponentStyle GetHeaderStyle()
        {
            return new CellTextComponentStyle()
            {
                ComponentWidth = pdfConfig.HeaderConfig.LeftTitleConfig.Width,
                TextAlignment = pdfConfig.HeaderConfig.LeftTitleConfig.TextAlignment,

                TextFont = FontFactory.GetFont(
                    pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.Name,
                    pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.Size,
                    pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.Style,

                    new BaseColor(
                        pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.HeaderConfig.LeftTitleConfig.FontConfig.ColorConfig.Blue)),
                XPosition = pdfConfig.HeaderConfig.LeftTitleConfig.XPosition,
                YPosition = pdfConfig.HeaderConfig.LeftTitleConfig.YPosition,

                Padding = new Padding()
                {
                    Bottom = pdfConfig.HeaderConfig.LeftTitleConfig.PaddingConfig.Bottom,
                    Left = pdfConfig.HeaderConfig.LeftTitleConfig.PaddingConfig.Left,
                    Right = pdfConfig.HeaderConfig.LeftTitleConfig.PaddingConfig.Right,
                    Top = pdfConfig.HeaderConfig.LeftTitleConfig.PaddingConfig.Top
                }
            };
        }

        private CellTextComponentStyle GetPageNumberStyle()
        {
            return new CellTextComponentStyle()
            {
                ComponentWidth = pdfConfig.FooterConfig.RightTitleConfig.Width,
                TextAlignment = pdfConfig.FooterConfig.RightTitleConfig.TextAlignment,

                TextFont = FontFactory.GetFont(
                    pdfConfig.FooterConfig.RightTitleConfig.FontConfig.Name,
                    pdfConfig.FooterConfig.RightTitleConfig.FontConfig.Size,
                    pdfConfig.FooterConfig.RightTitleConfig.FontConfig.Style,
                    new BaseColor(
                        pdfConfig.FooterConfig.RightTitleConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.FooterConfig.RightTitleConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.FooterConfig.RightTitleConfig.FontConfig.ColorConfig.Blue)),

                XPosition = pdfConfig.FooterConfig.RightTitleConfig.XPosition,
                YPosition = pdfConfig.FooterConfig.RightTitleConfig.YPosition,

                Padding = new Padding()
                {
                    Bottom = pdfConfig.FooterConfig.RightTitleConfig.PaddingConfig.Bottom,
                    Left = pdfConfig.FooterConfig.RightTitleConfig.PaddingConfig.Left,
                    Right = pdfConfig.FooterConfig.RightTitleConfig.PaddingConfig.Right,
                    Top = pdfConfig.FooterConfig.RightTitleConfig.PaddingConfig.Top
                }
            };
        }

        private CellTextComponentStyle GetDateStyle()
        {
            return new CellTextComponentStyle()
            {
                ComponentWidth = pdfConfig.HeaderConfig.RightTitleConfig.Width,
                TextAlignment = pdfConfig.HeaderConfig.RightTitleConfig.TextAlignment,

                TextFont = FontFactory.GetFont(
                    pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.Name,
                    pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.Size,
                    pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.Style,
                    new BaseColor(
                        pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.HeaderConfig.RightTitleConfig.FontConfig.ColorConfig.Blue)),

                XPosition = pdfConfig.HeaderConfig.RightTitleConfig.XPosition,
                YPosition = pdfConfig.HeaderConfig.RightTitleConfig.YPosition,

                Padding = new Padding()
                {
                    Bottom = pdfConfig.HeaderConfig.RightTitleConfig.PaddingConfig.Bottom,
                    Left = pdfConfig.HeaderConfig.RightTitleConfig.PaddingConfig.Left,
                    Right = pdfConfig.HeaderConfig.RightTitleConfig.PaddingConfig.Right,
                    Top = pdfConfig.HeaderConfig.RightTitleConfig.PaddingConfig.Top
                }
            };
        }

        private LineStyle GetLineStyle(float startX, float startY, float endX, float endY)
        {
            return new LineStyle()
            {
                LineColor = BaseColor.LIGHT_GRAY,
                StartX = startX,
                StartY = startY,
                EndX = endX,
                EndY = endY
            };
        }

        private CellTextComponentStyle GetSignatureStyle()
        {
            return new CellTextComponentStyle()
            {
                ComponentWidth = pdfConfig.SignatureConfig.Width,
                TextAlignment = pdfConfig.SignatureConfig.TextAlignment,

                TextFont = FontFactory.GetFont(
                    pdfConfig.SignatureConfig.FontConfig.Name,
                    pdfConfig.SignatureConfig.FontConfig.Size,
                    pdfConfig.SignatureConfig.FontConfig.Style,
                    new BaseColor(
                        pdfConfig.SignatureConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.SignatureConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.SignatureConfig.FontConfig.ColorConfig.Blue)),

                XPosition = pdfConfig.SignatureConfig.XPosition,
                YPosition = pdfConfig.SignatureConfig.YPosition,

                Padding = new Padding()
                {
                    Bottom = pdfConfig.SignatureConfig.PaddingConfig.Bottom,
                    Left = pdfConfig.SignatureConfig.PaddingConfig.Left,
                    Right = pdfConfig.SignatureConfig.PaddingConfig.Right,
                    Top = pdfConfig.SignatureConfig.PaddingConfig.Top
                }
            };
        }

        private TextComponentStyle GetProfitComponentStyle()
        {
            return new TextComponentStyle()
            {
                Alignment = pdfConfig.ProfitComponentConfig.Alignment,
                SpacingAfter = pdfConfig.ProfitComponentConfig.SpacingAfter,
                SpacingBefore = pdfConfig.ProfitComponentConfig.SpacingBefore,
                TextFont = FontFactory.GetFont(
                    pdfConfig.ProfitComponentConfig.FontConfig.Name,
                    pdfConfig.ProfitComponentConfig.FontConfig.Size,
                    pdfConfig.ProfitComponentConfig.FontConfig.Style,
                    new BaseColor(
                        pdfConfig.ProfitComponentConfig.FontConfig.ColorConfig.Red,
                        pdfConfig.ProfitComponentConfig.FontConfig.ColorConfig.Green,
                        pdfConfig.ProfitComponentConfig.FontConfig.ColorConfig.Blue))
            };
        }
    }
}