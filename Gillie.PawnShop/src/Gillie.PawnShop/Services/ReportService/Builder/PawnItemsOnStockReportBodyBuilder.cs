﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class PawnItemsOnStockReportBodyBuilder : AbstractReportBodyBuilder
    {
        public PawnItemsOnStockReportBodyBuilder(IReportComponentBuilder componentBuilder) 
            : base(componentBuilder, "PawnItemsInStock")
        {
            
        }

        public IReportComponent[] GetReportBodyComponents(IReportTableContent content)
        {
            return new IReportComponent[]
            {
                componentBuilder.GetBody(Key, content),
                componentBuilder.GetImageDrawer()
            };
        }
    }
}