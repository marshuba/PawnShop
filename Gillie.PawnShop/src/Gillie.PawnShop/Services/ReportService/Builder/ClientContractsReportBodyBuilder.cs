﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class ClientContractsReportBodyBuilder : AbstractReportBodyBuilder
    {
        public ClientContractsReportBodyBuilder(IReportComponentBuilder componentBuilder) 
            : base(componentBuilder, "ClientContractsReport")
        {
            
        }

        public IReportComponent[] GetReportBodyComponents(
            IReportTableContent content, 
            string headerContent)
        {
            return new IReportComponent[]
            {
                componentBuilder.GetSignature(headerContent),
                componentBuilder.GetBody(Key, content),
                componentBuilder.GetImageDrawer()
            };
        }
    }
}