﻿using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Builder
{
    public class ClientsReportBodyBuilder : AbstractReportBodyBuilder
    {
        public ClientsReportBodyBuilder(IReportComponentBuilder componentBuilder) 
            : base(componentBuilder, "ClientsReport")
        {
            
        }

        public IReportComponent[] GetReportBodyComponents(IReportTableContent content)
        {
            return new IReportComponent[]
            {
                componentBuilder.GetBody(Key, content),
                componentBuilder.GetImageDrawer()
            };
        }
    }
}