﻿using Gillie.PawnShop.Services.ClientService.Interfaces;
using Gillie.PawnShop.Services.ContractService.Interfaces;
using Gillie.PawnShop.Services.EmployeeService.Interfaces;
using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Services.ReportService.Builder;
using Gillie.PawnShop.Services.ReportService.Factory.Interfaces;
using Gillie.PawnShop.Services.ReportService.Reports.Component.Body.ClientReportContent;
using System;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using Gillie.PawnShop.Services.ReportService.Reports.Component.Body.EmployeeReportContent;
using Gillie.PawnShop.Services.ProfitCalculationService.Interfaces;
using Gillie.PawnShop.Services.ReportService.Reports.Component.Body.PawnItemReportContent;
using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;

namespace Gillie.PawnShop.Services.ReportService.Factory
{
    public class ReportFactory : IReportFactory
    {
        private readonly IReportBuilder reportBuilder;
        private readonly IContractService contractService;
        private readonly IEmployeeService employeeService;
        private readonly IPawnItemService pawnItemService;
        private readonly IClientService clientService;
        private readonly IProfitCalculationService profitCalculationService;

        public ReportFactory(
            IReportBuilder reportBuilder,
            IContractService contractService,
            IEmployeeService employeeService,
            IPawnItemService pawnItemService,
            IClientService clientService,
            IProfitCalculationService profitCalculationService)
        {
            this.reportBuilder = reportBuilder;
            this.contractService = contractService;
            this.employeeService = employeeService;
            this.pawnItemService = pawnItemService;
            this.clientService = clientService;
            this.profitCalculationService = profitCalculationService;
        }

        public IReport GetClientsReport()
        {
            var content = new ClientsReportContent(clientService);
            var bodyBuilder = new ClientsReportBodyBuilder(reportBuilder.ReportComponentBuilder);

            return reportBuilder.BuildReport(bodyBuilder.Key, bodyBuilder.GetReportBodyComponents(content));
        }

        public IReport GetClientContractsReport(string email)
        {
            var client = clientService.GetClientByEmail(email).Result;
            var content = new ClientContractsReportContent(client, contractService, pawnItemService);
            var bodyBuilder = new ClientContractsReportBodyBuilder(reportBuilder.ReportComponentBuilder);

            return reportBuilder.BuildReport(bodyBuilder.Key, bodyBuilder.GetReportBodyComponents(content, content.GetSignature()));
        }

        public IReport GetEmployeesReport()
        {
            var content = new EmployeesReportContent(employeeService);
            var bodyBuilder = new EmployeesReportBodyBuilder(reportBuilder.ReportComponentBuilder);

            return reportBuilder.BuildReport(bodyBuilder.Key, bodyBuilder.GetReportBodyComponents(content));
        }

        public IReport GetEmployeeProfitReport(string email, DateTime firstDate, DateTime lastDate)
        {
            var employee = employeeService.GetEmployeeByEmail(email).Result;
            var content = new EmployeeProfitReportContent(
                employee, firstDate, lastDate, pawnItemService, contractService, profitCalculationService);
            var bodyBuilder = new EmployeeProfitReportBodyBuilder(reportBuilder.ReportComponentBuilder);

            return reportBuilder.BuildReport(
                bodyBuilder.Key, 
                bodyBuilder.GetReportBodyComponents(content, 
                content.GetSignature(), 
                content.GetProfitInText()));
        }

        public IReport GetPawnItemsReport()
        {
            var content = new PawnItemsOnStockReportContent(pawnItemService);
            var bodyBuilder = new PawnItemsOnStockReportBodyBuilder(reportBuilder.ReportComponentBuilder);

            return reportBuilder.BuildReport(bodyBuilder.Key, bodyBuilder.GetReportBodyComponents(content));
        }
    }
}