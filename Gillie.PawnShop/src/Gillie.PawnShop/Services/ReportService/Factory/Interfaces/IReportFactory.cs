﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using System;

namespace Gillie.PawnShop.Services.ReportService.Factory.Interfaces
{
    public interface IReportFactory
    {
        IReport GetClientsReport();
        IReport GetClientContractsReport(string email);
        IReport GetEmployeesReport();
        IReport GetEmployeeProfitReport(string email, DateTime firstDate, DateTime lastDate);
        IReport GetPawnItemsReport();
    }
}