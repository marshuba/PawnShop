﻿namespace Gillie.PawnShop.Services.ReportService.Reports.Enums
{
    public enum ReportType
    {
        ClientsReport = 1,
        ClientContractReport,
        EmployeesReport,
        EmployeeProfitReport,
        PawnItemInStockReport
    }
}
