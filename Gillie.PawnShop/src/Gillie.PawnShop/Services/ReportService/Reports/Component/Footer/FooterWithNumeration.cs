﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.Reports.Component.General;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General;
using Gillie.PawnShop.Services.ReportService.ReportParts.Style;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Footer
{
    public class FooterWithNumeration : RunningTitle
    {
        private CellTextComponent pageText;
        private readonly CellTextComponentStyle pageTextStyle;

        public FooterWithNumeration(CellTextComponent titleText, LineDrawer lineDrawer, CellTextComponentStyle pageTextStyle) 
            : base(titleText, lineDrawer)
        {
            this.pageTextStyle = pageTextStyle;
        }

        public override void Print(PdfWriter writer, Document document)
        {
            base.Print(writer, document);
            pageText = new CellTextComponent(document.PageNumber.ToString(), pageTextStyle);
            pageText.Print(writer, document);
        }
    }
}
