﻿using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.General
{
    public class RunningTitle : IReportComponent
    {
        private readonly CellTextComponent titleText;
        private readonly LineDrawer lineDrawer;

        public RunningTitle(CellTextComponent titleText, LineDrawer lineDrawer)
        {
            this.titleText = titleText;
            this.lineDrawer = lineDrawer;
        }

        public virtual void Print(PdfWriter writer, Document document)
        {
            titleText.Print(writer, document);
            lineDrawer.Print(writer, document);
        }
    }
}
