﻿using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Parts.General;
using Gillie.PawnShop.Services.ReportService.ReportParts.Style;
using System.Globalization;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.General
{
    public class HeaderWithDate : RunningTitle
    {
        private readonly CellTextComponent dateText;

        public HeaderWithDate(CellTextComponent titleText, LineDrawer lineDrawer, CellTextComponentStyle dateStyle) 
            : base(titleText, lineDrawer)
        {
            var dateNow = DateTime.Now;
            var dateString = string.Format("{0}, {1}", dateNow.ToString("dd-MMM-yyyy",
                CultureInfo.InvariantCulture), dateNow.ToString("H:mm"));

            dateText = new CellTextComponent(dateString, dateStyle);
        }

        public override void Print(PdfWriter writer, Document document)
        {
            base.Print(writer, document);
            dateText.Print(writer, document);
        }
    }
}
