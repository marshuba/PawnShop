﻿using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Body.PawnItemReportContent
{
    public class PawnItemsOnStockReportContent : IReportTableContent
    {
        private readonly IPawnItemService pawnItemService;

        public PawnItemsOnStockReportContent(IPawnItemService pawnItemService)
        {
            this.pawnItemService = pawnItemService;
        }

        public string[] GetHeaderContent()
        {
            return new string[]
            {
                "Id",
                "Name",
                "Description",
                "Cost"
            };
        }

        public string[][] GetTableContent()
        {
            var items = pawnItemService.GetPawnItemsOnStock().Result;
            var content = new string[items.Length][];

            for (int i = 0; i < items.Length; i++)
            {
                content[i] = new string[4];
                content[i][0] = items[i].PawnItemId.ToString();
                content[i][1] = items[i].Name;
                content[i][2] = items[i].Description;
                content[i][3] = items[i].SellPrice.ToString();
            }

            return content;
        }
    }
}
