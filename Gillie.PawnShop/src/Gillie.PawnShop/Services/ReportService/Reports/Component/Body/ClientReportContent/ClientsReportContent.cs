﻿using Gillie.PawnShop.Services.ClientService.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Body.ClientReportContent
{
    public class ClientsReportContent : IReportTableContent
    {
        private readonly IClientService clientService;

        public ClientsReportContent(IClientService clientService)
        {
            this.clientService = clientService;
        }

        public string[] GetHeaderContent()
        {
            return new string[] { "Name", "Surname", "Email", "Phone Number" };
        }

        public string[][] GetTableContent()
        {
            var clients = clientService.GetClientsAsync().Result;

            string[][] content = new string[clients.Length][];
            for (int i = 0; i < clients.Length; i++)
            {
                content[i] = new string[4];
                content[i][0] = clients[i].Name;
                content[i][1] = clients[i].Surname;
                content[i][2] = clients[i].Email;
                content[i][3] = clients[i].PhoneNumber;
            }

            return content;
        }
    }
}
