﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Entities.Enums;
using Gillie.PawnShop.Services.ContractService.Interfaces;
using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Services.ProfitCalculationService.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;
using System;
using System.Globalization;
using System.Text;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Body.EmployeeReportContent
{
    public class EmployeeProfitReportContent : IReportTableContent
    {
        private readonly Employee employee;
        private readonly IPawnItemService pawnItemService;
        private readonly IContractService contractService;
        private readonly IProfitCalculationService profitCalculationService;
        private readonly DateTime firstDate;
        private readonly DateTime lastDate;

        public EmployeeProfitReportContent(
            Employee employee,
            DateTime firstDate,
            DateTime lastDate,
            IPawnItemService pawnItemService,
            IContractService contractService,
            IProfitCalculationService profitCalculationService)
        {
            this.employee = employee;
            this.firstDate = firstDate;
            this.lastDate = lastDate;
            this.pawnItemService = pawnItemService;
            this.contractService = contractService;
            this.profitCalculationService = profitCalculationService;
        }

        public string[] GetHeaderContent()
        {
            return new string[]
            {
                "Contract Id",
                "Item Name",
                "Description",
                "Item State",
                "Income"
            };
        }

        public string GetSignature()
        {
            var builder = new StringBuilder(200);
            builder.Append("Employee: ");
            builder.AppendFormat("{0} {1}\n", employee.Name, employee.Surname);
            builder.AppendFormat("Email: {0}\nPhone Number: {1}\n", employee.Email, employee.PhoneNumber);
            builder.AppendFormat("From: {0} To: {1}", firstDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture),
                lastDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture));

            return builder.ToString();
        }

        public string GetProfitInText()
        {
            return profitCalculationService.GetProfitByEmployeeInPeriod(employee, firstDate, lastDate).Result.ToString();
        }

        public string[][] GetTableContent()
        {
            var contracts = contractService.GetContractByEmployeeAsync(employee, firstDate, lastDate).Result;
            var pawnItems = pawnItemService.GetPawnItemsByContracts(contracts).Result;
            var content = new string[contracts.Length][];

            for (int i = 0; i < contracts.Length; i++)
            {
                content[i] = new string[5];
                content[i][0] = contracts[i].ContractId.ToString();
                content[i][1] = pawnItems[i].Name;
                content[i][2] = pawnItems[i].Description;
                content[i][3] = GetItemState(pawnItems[i].PawnItemState);
                content[i][4] = GetIncome(contracts[i], pawnItems[i]).ToString();
            }

            return content;
        }

        private string GetItemState(PawnItemState itemState)
        {
            switch (itemState)
            {
                case PawnItemState.ReturnedToClient:
                    return "Returned";
                case PawnItemState.OnStockBelongClient:
                    return "On Stock";
                case PawnItemState.OnStockBelongShop:
                    return "On Stock";
            }

            return "Sold";
        }

        private double GetIncome(Contract contract, PawnItem pawnItem)
        {
            double income = 0;
            switch (pawnItem.PawnItemState)
            {
                case PawnItemState.ReturnedToClient:
                    income = pawnItem.BuyPrice * contract.Percents / 100;
                    break;
                case PawnItemState.OnStockBelongShop:
                    income = -pawnItem.BuyPrice;
                    break;
                case PawnItemState.OnStockBelongClient:
                    income = -pawnItem.BuyPrice;
                    break;
                case PawnItemState.Sold:
                    income = pawnItem.SellPrice;
                    break;
            }
            return income;
        }
    }
}
