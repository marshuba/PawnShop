﻿using Gillie.PawnShop.Services.EmployeeService.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Body.EmployeeReportContent
{
    public class EmployeesReportContent : IReportTableContent
    {
        private readonly IEmployeeService employeeService;

        public EmployeesReportContent(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        public string[] GetHeaderContent()
        {
            return new string[] { "Name", "Surname", "Email", "Phone Number" };
        }

        public string[][] GetTableContent()
        {
            var employees = employeeService.GetEmployees().Result;

            string[][] content = new string[employees.Length][];
            for (int i = 0; i < employees.Length; i++)
            {
                content[i] = new string[4];
                content[i][0] = employees[i].Name;
                content[i][1] = employees[i].Surname;
                content[i][2] = employees[i].Email;
                content[i][3] = employees[i].PhoneNumber;
            }

            return content;
        }
    }
}
