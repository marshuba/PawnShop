﻿using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Entities.Enums;
using Gillie.PawnShop.Services.ContractService.Interfaces;
using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Services.ReportService.ReportEntities.Interfaces.Content;
using System.Globalization;
using System.Text;

namespace Gillie.PawnShop.Services.ReportService.Reports.Component.Body.ClientReportContent
{
    public class ClientContractsReportContent : IReportTableContent
    {
        private readonly Client client;
        private readonly IContractService contractService;
        private readonly IPawnItemService pawnItemService;

        public ClientContractsReportContent(
            Client client, 
            IContractService contractService, 
            IPawnItemService pawnItemService)
        {
            this.client = client;
            this.contractService = contractService;
            this.pawnItemService = pawnItemService;
        }

        public string[] GetHeaderContent()
        {
            return new string[]
            {
                "Id",
                "Date Start",
                "Date End",
                "Is Closed",
                "Item",
                "Item State"
            };
        }

        public string GetSignature()
        {
            var builder = new StringBuilder(200);
            builder.Append("Client: ");
            builder.AppendFormat("{0} {1}\n", client.Name, client.Surname);
            builder.AppendFormat("Email: {0}\nPhone Number: {1}", client.Email, client.PhoneNumber);

            return builder.ToString();
        }

        public string[][] GetTableContent()
        {
            var contracts = contractService.GetContractsByClientAsync(client).Result;
            var pawnItems = pawnItemService.GetPawnItemsByContracts(contracts).Result;

            var content = new string[contracts.Length][];
            for (int i = 0; i < contracts.Length; i++)
            {
                content[i] = new string[6];
                content[i][0] = contracts[i].ContractId.ToString();
                content[i][1] = contracts[i].DateStart.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
                content[i][2] = contracts[i].DateEnd.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
                content[i][3] = contracts[i].IsClosed == true ? "+" : "-";
                content[i][4] = pawnItems[i].Name;
                content[i][5] = GetItemState(pawnItems[i].PawnItemState);
            }

            return content;
        }

        private string GetItemState(PawnItemState itemState)
        {
            switch (itemState)
            {
                case PawnItemState.ReturnedToClient:
                    return "Returned";
                case PawnItemState.OnStockBelongClient | 
                PawnItemState.OnStockBelongShop:
                    return "On Stock";
            }

            return "Sold";
        }
    }
}