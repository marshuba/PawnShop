﻿using Gillie.PawnShop.Services.ProfitCalculationService.Interfaces;
using System;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Services.ContractService.Interfaces;
using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Services.ProfitCalculationService
{
    public class ProfitCalculationService : IProfitCalculationService
    {
        private readonly IContractService contractService;
        private readonly IPawnItemService pawnItemService;

        public ProfitCalculationService(
            IContractService contractService, 
            IPawnItemService pawnItemService)
        {
            this.contractService = contractService;
            this.pawnItemService = pawnItemService;
        }

        public async Task<double> GetProfitByEmployeeInPeriod(Employee employee, DateTime firstDate, DateTime lastDate)
        {
            var contracts = await contractService.GetContractByEmployeeAsync(employee, firstDate, lastDate);
            var items = await pawnItemService.GetPawnItemsByContracts(contracts);

            var profit = 0.0;
            for (int i = 0; i < contracts.Length; i++)
            {
                profit += GetIncome(contracts[i], items[i]);
            }

            return profit;
        }

        private double GetIncome(Contract contract, PawnItem pawnItem)
        {
            double income = 0;
            switch (pawnItem.PawnItemState)
            {
                case PawnItemState.ReturnedToClient:
                    income = pawnItem.BuyPrice * contract.Percents / 100;
                    break;
                case PawnItemState.OnStockBelongShop:
                    income = -pawnItem.BuyPrice;
                    break;
                case PawnItemState.OnStockBelongClient:
                    income = -pawnItem.BuyPrice;
                    break;
                case PawnItemState.Sold:
                    income = pawnItem.SellPrice;
                    break;
            }
            return income;
        }
    }
}
