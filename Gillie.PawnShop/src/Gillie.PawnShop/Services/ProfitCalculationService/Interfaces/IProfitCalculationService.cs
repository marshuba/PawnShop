﻿using Gillie.PawnShop.Entities;
using System;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.ProfitCalculationService.Interfaces
{
    public interface IProfitCalculationService
    {
        Task<double> GetProfitByEmployeeInPeriod(Employee employee, DateTime firstDate, DateTime lastDate);
    }
}
