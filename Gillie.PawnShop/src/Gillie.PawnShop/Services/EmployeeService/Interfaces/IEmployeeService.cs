﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.EmployeeService.Interfaces
{
    public interface IEmployeeService
    {
        Task<Employee[]> GetEmployees();
        Task<Employee> GetEmployeeByEmail(string email);
    }
}
