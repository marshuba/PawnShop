﻿using Gillie.PawnShop.Services.EmployeeService.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.EmployeeRepository.Interfaces;

namespace Gillie.PawnShop.Services.EmployeeService
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeReadRepository employeeReadRepository;

        public EmployeeService(IEmployeeReadRepository employeeReadRepository)
        {
            this.employeeReadRepository = employeeReadRepository;
        }

        public async Task<Employee> GetEmployeeByEmail(string email)
        {
            return await employeeReadRepository.GetEmployeeByEmailAsync(email);
        }

        public async Task<Employee[]> GetEmployees()
        {
            return await employeeReadRepository.GetEmployeesAsync();
        }
    }
}
