﻿using Gillie.PawnShop.Services.ClientService.Interfaces;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;

namespace Gillie.PawnShop.Services.ClientService
{
    public class ClientService : IClientService
    {
        private readonly IClientReadRepository clientReadRepository;

        public ClientService(IClientReadRepository clientReadRepository)
        {
            this.clientReadRepository = clientReadRepository;
        }

        public async Task<Client[]> GetClientsAsync()
        {
            return await clientReadRepository.GetClientsAsync();
        }

        public async Task<Client> GetClientByEmail(string email)
        {
            return await clientReadRepository.GetClientByEmailAsync(email);
        }
    }
}
