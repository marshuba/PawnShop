﻿using Gillie.PawnShop.Entities;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.ClientService.Interfaces
{
    public interface IClientService
    {
        Task<Client[]> GetClientsAsync();
        Task<Client> GetClientByEmail(string email);
    }
}
