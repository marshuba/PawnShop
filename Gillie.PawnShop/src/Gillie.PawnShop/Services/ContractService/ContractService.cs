﻿using Gillie.PawnShop.Services.ContractService.Interfaces;
using System;
using System.Threading.Tasks;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.ContractRepository.Interfaces;

namespace Gillie.PawnShop.Services.ContractService
{
    public class ContractService : IContractService
    {
        private readonly IContractReadRepository contractReadRepository;

        public ContractService(IContractReadRepository contractReadRepository)
        {
            this.contractReadRepository = contractReadRepository;
        }

        public async Task<Contract[]> GetContractByEmployeeAsync(Employee employee)
        {
            return await contractReadRepository.GetContractsByEmployeeAsync(employee);
        }

        public async Task<Contract[]> GetContractByEmployeeAsync(Employee employee, DateTime firstDate, DateTime lastDate)
        {
            return await contractReadRepository.GetContractsByEmployeeAsync(employee, firstDate, lastDate);
        }

        public async Task<Contract[]> GetContractsAsync()
        {
            return await contractReadRepository.GetContractsAsync();
        }

        public async Task<Contract[]> GetContractsByClientAsync(Client client)
        {
            return await contractReadRepository.GetContractsByClientAsync(client);
        }
    }
}
