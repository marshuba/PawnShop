﻿using Gillie.PawnShop.Entities;
using System;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Services.ContractService.Interfaces
{
    public interface IContractService
    {
        Task<Contract[]> GetContractsAsync();
        Task<Contract[]> GetContractsByClientAsync(Client client);
        Task<Contract[]> GetContractByEmployeeAsync(Employee employee);
        Task<Contract[]> GetContractByEmployeeAsync(Employee employee, DateTime firstDate, DateTime lastDate);
    }
}