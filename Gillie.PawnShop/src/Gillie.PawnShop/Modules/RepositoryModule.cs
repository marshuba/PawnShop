﻿using Autofac;
using Gillie.PawnShop.Context;
using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Repository.BranchRepositories;
using Gillie.PawnShop.Repository.BranchRepositories.Interfaces;
using Gillie.PawnShop.Repository.BranchLocationRepository;
using Gillie.PawnShop.Repository.BranchLocationRepository.Interfaces;
using Gillie.PawnShop.Repository.BranchWorkTimeRepository;
using Gillie.PawnShop.Repository.BranchWorkTimeRepository.Interfaces;
using Gillie.PawnShop.Repository.ClientRepository;
using Gillie.PawnShop.Repository.ClientRepository.Interfaces;
using Gillie.PawnShop.Repository.CompanyRepository;
using Gillie.PawnShop.Repository.CompanyRepository.Interfaces;
using Gillie.PawnShop.Repository.HistoryRecordsRepository;
using Gillie.PawnShop.Repository.HistoryRecordsRepository.Interfaces;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Repository.EmployeeRepository;
using Gillie.PawnShop.Repository.EmployeeRepository.Interfaces;
using Gillie.PawnShop.Repository.ContractRepository;
using Gillie.PawnShop.Repository.ContractRepository.Interfaces;
using Gillie.PawnShop.Repository.ClientLocationRepository;
using Gillie.PawnShop.Repository.ClientLocationRepository.Interfaces;
using Gillie.PawnShop.Repository.PawnItemRepository;
using Gillie.PawnShop.Repository.PawnItemRepository.Interfaces;
using Gillie.PawnShop.Repository.CompanyReviewRepository;
using Gillie.PawnShop.Repository.CompanyReviewRepository.Interfaces;

namespace Gillie.PawnShop.Modules
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PawnshopContext>()
                .As<DbContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<BranchReadRepository>().As<IBranchReadRepository>();
            builder.RegisterType<BranchWriteRepository>().As<IBranchWriteRepository>();

            builder.RegisterType<BranchLocationReadRepository>().As<IBranchLocationReadRepository>();
            builder.RegisterType<BranchLocationWriteRepository>().As<IBranchLocationWriteRepository>();

            builder.RegisterType<BranchWorkTimeReadRepository>().As<IBranchWorkTimeReadRepository>();
            builder.RegisterType<BranchWorkTimeWriteRepository>().As<IBranchWorkTimeWriteRepository>();

            builder.RegisterType<CompanyReadRepository>().As<ICompanyReadRepository>();

            builder.RegisterType<HistoryRecordsReadRepository>().As<IHistoryRecordsReadRepository>();
            builder.RegisterType<HistoryRecordsWriteRepository>().As<IHistoryRecordsWriteRepository>();
            
            builder.RegisterType<ContractReadRepository>().As<IContractReadRepository>();
            builder.RegisterType<ContractWriteRepository>().As<IContractWriteRepository>();

            builder.RegisterType<ClientLocationReadRepository>().As<IClientLocationReadRepository>();
            builder.RegisterType<ClientLocationWriteRepository>().As<IClientLocationWriteRepository>();

            builder.RegisterType<ClientWriteRepository>().As<IClientWriteRepository>();
            builder.RegisterType<ClientReadRepository>().As<IClientReadRepository>();

            builder.RegisterType<EmployeeReadRepository>().As<IEmployeeReadRepository>();
            builder.RegisterType<EmployeeWriteRepository>().As<IEmployeeWriteRepository>();

            builder.RegisterType<PawnItemReadRepository>().As<IPawnItemReadRepository>();
            builder.RegisterType<PawnItemWriteRepository>().As<IPawnItemWriteRepository>();

            builder.RegisterType<CompanyReviewReadRepository>().As<ICompanyReviewReadRepository>();
            builder.RegisterType<CompanyReviewWriteRepository>().As<ICompanyReviewWriteRepository>();
        }
    }
}