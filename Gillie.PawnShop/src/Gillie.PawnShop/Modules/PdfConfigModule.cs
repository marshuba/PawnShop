﻿using Autofac;
using Gillie.PawnShop.Configuration.PdfConfig.Component;
using Gillie.PawnShop.Configuration.PdfConfig;
using Gillie.PawnShop.Configuration;
using System.IO;
using Newtonsoft.Json;
using System;
using Gillie.PawnShop.Configuration.PdfConfig.Report;
using System.Collections.Generic;
using System.Linq;

namespace Gillie.PawnShop.Modules
{
    public class PdfConfigModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(CreatePdfConfig).InstancePerLifetimeScope();
        }

        private static IPdfConfig CreatePdfConfig(IComponentContext context)
        {
            var path = "PdfConfig.json";
            var fileContent = File.ReadAllText(path);

            var pdf = JsonConvert.DeserializeObject<PdfConfig>(fileContent);
            return pdf;
        }

        private class TypeConverter<TConcreteType> : JsonConverter
        {
            public override bool CanConvert(Type objectType) => true;

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                return serializer.Deserialize<TConcreteType>(reader);
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                serializer.Serialize(writer, value);
            }
        }

        private class PdfColorConfig : IPdfColorConfig
        {
            public int Blue { get; set; }
            public int Green { get; set; }
            public int Red { get; set; }
        }

        private class PdfComponentPaddingConfig : IPdfComponentPaddingConfig
        {
            public float Bottom { get; set; }
            public float Left { get; set; }
            public float Right { get; set; }
            public float Top { get; set; }
        }

        private class PdfImageConfig : IPdfImageConfig
        {
            public int Alignment { get; set; }
            public string ImagePath { get; set; }
            public float ScalePercent { get; set; }
        }

        private class PdfFontConfig : IPdfFontConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfColorConfig>))]
            public IPdfColorConfig ColorConfig { get; set; }
            public string Name { get; set; }
            public float Size { get; set; }
            public int Style { get; set; }
        }

        private class PdfComponentConfig : IPdfComponentConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfFontConfig>))]
            public IPdfFontConfig FontConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfComponentPaddingConfig>))]
            public IPdfComponentPaddingConfig PaddingConfig { get; set; }

            public int TextAlignment { get; set; }
            public float Width { get; set; }
            public float XPosition { get; set; }
            public float YPosition { get; set; }
        }

        private class PdfLineConfig : IPdfLineConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfColorConfig>))]
            public IPdfColorConfig ColorConfig { get; set; }

            public float EndX { get; set; }
            public float EndY { get; set; }
            public float StartX { get; set; }
            public float StartY { get; set; }
        }

        private class PdfBodyConfig : IPdfBodyConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfColorConfig>))]
            public IPdfColorConfig EvenRowsColorConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfColorConfig>))]
            public IPdfColorConfig OddRowsColorConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfFontConfig>))]
            public IPdfFontConfig TableFontConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfColorConfig>))]
            public IPdfColorConfig TableHeaderColorConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfFontConfig>))]
            public IPdfFontConfig TableHeaderFontConfig { get; set; }

            public float SpacingAfter { get; set; }
            public float SpacingBefore { get; set; }
        }

        private class PdfTitleConfig : IPdfTitleConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfComponentConfig>))]
            public IPdfComponentConfig LeftTitleConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfComponentConfig>))]
            public IPdfComponentConfig RightTitleConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfLineConfig>))]
            public IPdfLineConfig LineConfig { get; set; }
        }

        private class PdfTextParagraphConfig : IPdfTextParagraphConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfFontConfig>))]
            public IPdfFontConfig FontConfig { get; set; }

            public int Alignment { get; set; }
            public string FormatText { get; set; }
            public float SpacingAfter { get; set; }
            public float SpacingBefore { get; set; }
        }

        private class PdfMarginReportConfig : IPdfMarginReportConfig
        {
            public float Bottom { get; set; }
            public float Left { get; set; }
            public float Right { get; set; }
            public float Top { get; set; }
        }

        private class PdfReportConfig : IPdfReportConfig
        {
            [JsonConverter(typeof(TypeConverter<PdfMarginReportConfig>))]
            public IPdfMarginReportConfig MarginConfig { get; set; }

            public string FooterText { get; set; }
            public string HeaderText { get; set; }
            public float[] RelativeWidths { get; set; }
            public string ReportName { get; set; }
            public float SpacingBefore { get; set; }
            public float SpacingAfter { get; set; }

        }

        private class PdfConfig : IPdfConfig
        {
            private Dictionary<string, PdfReportConfig> dictionary = 
                new Dictionary<string, PdfReportConfig>();

            [JsonConverter(typeof(TypeConverter<PdfBodyConfig>))]
            public IPdfBodyConfig BodyConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfTitleConfig>))]
            public IPdfTitleConfig FooterConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfTitleConfig>))]
            public IPdfTitleConfig HeaderConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfImageConfig>))]
            public IPdfImageConfig ImageConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfComponentConfig>))]
            public IPdfComponentConfig SignatureConfig { get; set; }

            [JsonConverter(typeof(TypeConverter<PdfTextParagraphConfig>))]
            public IPdfTextParagraphConfig ProfitComponentConfig { get; set; }

            public IDictionary<string, IPdfReportConfig> ReportConfigs
            {
                get { return GetIDictionary(); }
            }

            public Dictionary<string, PdfReportConfig> ReportConfigurations
            {
                get { return dictionary; }
            }

            private IDictionary<string, IPdfReportConfig> GetIDictionary()
            {
                return dictionary.Cast<dynamic>()
                    .ToDictionary(
                        entry => (string)entry.Key, 
                        entry => (IPdfReportConfig)entry.Value);
            }
        }
    }
}