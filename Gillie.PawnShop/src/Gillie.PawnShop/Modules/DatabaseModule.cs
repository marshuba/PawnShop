﻿using Autofac;
using Gillie.PawnShop.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Gillie.PawnShop.Modules
{
    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(CreateDbContextOptions).As<DbContextOptions<PawnshopContext>>();
        }

        private static DbContextOptions<PawnshopContext> CreateDbContextOptions(IComponentContext context)
        {
            var config = context.Resolve<IConfiguration>();
            string connectionString = config["Data:DefaultConnection:ConnectionString"];
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<PawnshopContext>().
                UseNpgsql(connectionString);

            return dbContextOptionsBuilder.Options;
        }
    }
}
