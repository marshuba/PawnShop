﻿using Autofac;
using Gillie.PawnShop.Services;
using Gillie.PawnShop.Services.ClientService;
using Gillie.PawnShop.Services.ClientService.Interfaces;
using Gillie.PawnShop.Services.ContractService;
using Gillie.PawnShop.Services.ContractService.Interfaces;
using Gillie.PawnShop.Services.EmployeeService;
using Gillie.PawnShop.Services.EmployeeService.Interfaces;
using Gillie.PawnShop.Services.Interfaces;
using Gillie.PawnShop.Services.PawnItemService;
using Gillie.PawnShop.Services.PawnItemService.Interfaces;
using Gillie.PawnShop.Services.ProfitCalculationService;
using Gillie.PawnShop.Services.ProfitCalculationService.Interfaces;
using Gillie.PawnShop.Services.ReportService;
using Gillie.PawnShop.Services.ReportService.Builder;
using Gillie.PawnShop.Services.ReportService.Builder.Interfaces;
using Gillie.PawnShop.Services.ReportService.Factory;
using Gillie.PawnShop.Services.ReportService.Factory.Interfaces;
using Gillie.PawnShop.Services.ReportService.Interfaces;

namespace Gillie.PawnShop.Modules
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClientService>().As<IClientService>();
            builder.RegisterType<EmployeeService>().As<IEmployeeService>();
            builder.RegisterType<ContractService>().As<IContractService>();
            builder.RegisterType<PawnItemService>().As<IPawnItemService>();
            builder.RegisterType<ProfitCalculationService>().As<IProfitCalculationService>();

            builder.RegisterType<RegisterService>().As<IRegisterService>();
            builder.RegisterType<ReportComponentBuilder>().As<IReportComponentBuilder>();
            builder.RegisterType<ReportBuilder>().As<IReportBuilder>();
            builder.RegisterType<ReportFactory>().As<IReportFactory>();
            builder.RegisterType<ReportService>().As<IReportService>();
        }
    }
}