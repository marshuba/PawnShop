﻿using System.Collections.Generic;

namespace Gillie.PawnShop.Entities
{
    public class Client
    {
        public long ClientId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ClientLocation ClientLocation { get; set; }
        public virtual ICollection<CompanyReview> Reviews { get; set; }
        public virtual ICollection<ClientOperationHistory> ClientOperationHistories { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}

