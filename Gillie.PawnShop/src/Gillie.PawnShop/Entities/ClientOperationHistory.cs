﻿using Gillie.PawnShop.Entities.Enums;
using System;

namespace Gillie.PawnShop.Entities
{
    public class ClientOperationHistory
    {
        public long ClientOperationHistoryId { get; set; }
        public long ClientId { get; set; }
        public string Name { get; set; }
        public ClientOperationType ClientOperationType { get; set; }
        public DateTime DateOperation { get; set; }
        public string Description { get; set; }

        public virtual Client Client { get; set; }
    }
}
