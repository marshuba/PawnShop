﻿using System;

namespace Gillie.PawnShop.Entities
{
    public class CompanyReview
    {
        public long CompanyReviewId { get; set; }
        public long ClientId { get; set; }
        public DateTime ReviewDate { get; set; }
        public string Description { get; set; }
        public bool Publish { get; set; }

        public virtual Client Client { get; set; }
    }
}
