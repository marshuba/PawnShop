﻿using System.Collections.Generic;

namespace Gillie.PawnShop.Entities
{
    public class Company
    {
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public virtual ICollection<HistoryRecord> HistoryRecordes { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
