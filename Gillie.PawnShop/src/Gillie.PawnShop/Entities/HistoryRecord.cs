using System;

namespace Gillie.PawnShop.Entities
{
    public class HistoryRecord
    {
        public long HistoryRecordId { get; set; }
        public long HistoryRecordTypeId { get; set; }
        public long CompanyId { get; set; }
        public DateTime HistoryRecordDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual HistoryRecordType HistoryRecordType { get; set; }
    }
}

