﻿using Gillie.PawnShop.Entities.Enums;
using System;

namespace Gillie.PawnShop.Entities
{
    public class EmployeeOperationHistory
    {
        public long EmployeeOperationHistoryId { get; set; }
        public long EmployeeId { get; set; }
        public string Name { get; set; }
        public EmployeeOperationType EmployeeOperationType { get; set; }
        public DateTime DateOperation { get; set; }
        public string Description { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
