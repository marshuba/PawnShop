﻿namespace Gillie.PawnShop.Entities
{
    public class ClientLocation
    {
        public long ClientLocationId { get; set; }
        public long ClientId { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public virtual Client Client { get; set; }
    }
}
