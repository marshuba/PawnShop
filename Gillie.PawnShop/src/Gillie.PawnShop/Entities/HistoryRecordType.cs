﻿using System.Collections.Generic;

namespace Gillie.PawnShop.Entities
{
    public class HistoryRecordType
    {
        public long HistoryRecordTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<HistoryRecord> HistoryRecords { get; set; }
    }
}