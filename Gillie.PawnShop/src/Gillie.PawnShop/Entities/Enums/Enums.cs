﻿namespace Gillie.PawnShop.Entities.Enums
{
    public enum BranchType
    {
        PawnShop = 1,
        PawnStore
    }

    public enum ClientOperationType
    {
        SignContract,
        CloseContract,
        BuyPawnItem
    }

    public enum EmployeeType
    {
        PawnWorker = 1,
        PawnSeller,
        Admin
    }

    public enum EmployeeOperationType
    {
        OpenAccount = 1,
        CreateContract,
        CloseContract,
        SellPawnItem
    }

    public enum RecordType
    {
        News = 1,
        Promotion = 2,
        Bonus = 3,
        Promo = 4,
        Review = 5,
        Service = 6
    }

    public enum PawnItemState
    {
        ReturnedToClient = 1,
        OnStockBelongClient,
        OnStockBelongShop,
        Sold
    }
}
