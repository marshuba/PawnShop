﻿using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Entities
{
    public class PawnItem
    {
        public long PawnItemId { get; set; }
        public long ContractId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double BuyPrice { get; set; }
        public double SellPrice { get; set; }
        public PawnItemState PawnItemState { get; set; }

        public virtual Contract Contract { get; set; }
    }
}