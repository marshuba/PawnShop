﻿using System;

namespace Gillie.PawnShop.Entities
{
    public class Contract
    {
        public long ContractId { get; set; }
        public long ClientId { get; set; }
        public long EmployeeId { get; set; }
        public double Percents { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool IsClosed { get; set; }

        public virtual Client Client { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual PawnItem PawnItem { get; set; }
    }
}
