﻿using Gillie.PawnShop.Entities.Enums;
using System.Collections.Generic;

namespace Gillie.PawnShop.Entities
{
    public class Branch
    {
        public long BranchId { get; set; }
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public BranchType BranchType { get; set; }

        public virtual BranchLocation BranchLocation { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<BranchWorkTime> BranchWorkTimes { get; set; }
    }
}
