﻿namespace Gillie.PawnShop.Entities
{
    public class BranchLocation
    {
        public long BranchLocationId { get; set; }
        public long BranchId { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
