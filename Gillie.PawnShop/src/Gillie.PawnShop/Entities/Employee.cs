﻿using Gillie.PawnShop.Entities.Enums;
using System.Collections.Generic;

namespace Gillie.PawnShop.Entities
{
    public class Employee
    {
        public long EmployeeId { get; set; }
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public EmployeeType EmployeeType { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<EmployeeOperationHistory> EmployeeOperationHistories { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
