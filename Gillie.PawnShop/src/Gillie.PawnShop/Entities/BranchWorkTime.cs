﻿using System;

namespace Gillie.PawnShop.Entities
{
    public class BranchWorkTime
    {
        public long BranchWorkTimeId { get; set; }
        public long BranchId { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Day { get; set; }

        public virtual Branch Branch { get; set; }
    }
}