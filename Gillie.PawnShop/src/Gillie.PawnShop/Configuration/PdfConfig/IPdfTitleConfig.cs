﻿using Gillie.PawnShop.Configuration.PdfConfig.Component;

namespace Gillie.PawnShop.Configuration.PdfConfig
{
    public interface IPdfTitleConfig
    {
        IPdfComponentConfig LeftTitleConfig { get; }
        IPdfComponentConfig RightTitleConfig { get; }
        IPdfLineConfig LineConfig { get; }
    }
}