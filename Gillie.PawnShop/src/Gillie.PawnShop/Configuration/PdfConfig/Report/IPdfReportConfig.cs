﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Report
{
    public interface IPdfReportConfig
    {
        float[] RelativeWidths { get; }
        string ReportName { get; }
        string FooterText { get; }
        string HeaderText { get; }
        float SpacingBefore { get; }
        float SpacingAfter { get; }
        IPdfMarginReportConfig MarginConfig { get; }
    }
}