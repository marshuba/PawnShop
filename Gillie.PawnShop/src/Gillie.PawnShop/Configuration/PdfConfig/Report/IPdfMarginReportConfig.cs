﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Report
{
    public interface IPdfMarginReportConfig
    {
        float Bottom { get; }
        float Right { get; }
        float Top{ get; }
        float Left { get; }
    }
}
