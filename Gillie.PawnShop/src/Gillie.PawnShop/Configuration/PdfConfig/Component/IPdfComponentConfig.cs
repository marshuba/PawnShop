﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfComponentConfig
    {
        float Width { get; }
        int TextAlignment { get; }
        float XPosition { get; }
        float YPosition { get; }
        IPdfFontConfig FontConfig { get; }
        IPdfComponentPaddingConfig PaddingConfig { get; }
    }
}