﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfComponentPaddingConfig
    {
        float Bottom { get; }
        float Left { get; }
        float Right { get; }
        float Top { get; }
    }
}