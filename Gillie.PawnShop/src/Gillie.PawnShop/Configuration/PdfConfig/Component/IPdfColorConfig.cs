﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfColorConfig
    {
        int Red { get; }
        int Green { get; }
        int Blue { get; }
    }
}