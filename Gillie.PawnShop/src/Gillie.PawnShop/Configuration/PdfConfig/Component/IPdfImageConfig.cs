﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfImageConfig
    {
        string ImagePath { get; }
        int Alignment { get; }
        float ScalePercent { get; }
    }
}