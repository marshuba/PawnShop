﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfLineConfig
    {
        float StartX { get; }
        float StartY { get; }
        float EndX { get; }
        float EndY { get; }
        IPdfColorConfig ColorConfig { get; }
    }
}