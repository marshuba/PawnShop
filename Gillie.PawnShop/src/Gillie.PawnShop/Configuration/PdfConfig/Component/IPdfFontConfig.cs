﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfFontConfig
    {
        string Name { get; }
        float Size { get; }
        int Style { get; }
        IPdfColorConfig ColorConfig { get; }
    }
}