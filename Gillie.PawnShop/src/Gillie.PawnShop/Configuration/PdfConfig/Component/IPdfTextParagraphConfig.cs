﻿namespace Gillie.PawnShop.Configuration.PdfConfig.Component
{
    public interface IPdfTextParagraphConfig
    {
        string FormatText { get; }
        int Alignment { get; }
        float SpacingAfter { get; }
        float SpacingBefore { get; }
        IPdfFontConfig FontConfig { get; }
    }
}