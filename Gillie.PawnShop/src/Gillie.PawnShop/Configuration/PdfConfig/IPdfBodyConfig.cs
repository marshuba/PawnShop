﻿using Gillie.PawnShop.Configuration.PdfConfig.Component;

namespace Gillie.PawnShop.Configuration.PdfConfig
{
    public interface IPdfBodyConfig
    {
        IPdfFontConfig TableHeaderFontConfig { get; }
        IPdfFontConfig TableFontConfig { get; }
        IPdfColorConfig TableHeaderColorConfig { get; }
        IPdfColorConfig EvenRowsColorConfig { get; }
        IPdfColorConfig OddRowsColorConfig { get; }
    }
}