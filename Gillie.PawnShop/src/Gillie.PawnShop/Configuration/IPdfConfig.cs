﻿using Gillie.PawnShop.Configuration.PdfConfig;
using Gillie.PawnShop.Configuration.PdfConfig.Component;
using Gillie.PawnShop.Configuration.PdfConfig.Report;
using System.Collections.Generic;

namespace Gillie.PawnShop.Configuration
{
    public interface IPdfConfig
    {
        IPdfTitleConfig HeaderConfig { get; }
        IPdfBodyConfig BodyConfig { get; }
        IPdfTitleConfig FooterConfig { get; }
        IPdfComponentConfig SignatureConfig { get; }
        IPdfImageConfig ImageConfig { get; }
        IPdfTextParagraphConfig ProfitComponentConfig { get; }
        IDictionary<string, IPdfReportConfig> ReportConfigs { get; }
    }
}