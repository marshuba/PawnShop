﻿using Microsoft.EntityFrameworkCore;
using Gillie.PawnShop.Entities;
using Gillie.PawnShop.Context.Mappings;
using Gillie.PawnShop.Context.IdentityMapping;
using Gillie.PawnShop.Authorization.Entities.Identity;

namespace Gillie.PawnShop.Context
{
    public class PawnshopContext : DbContext
    {
        public PawnshopContext(DbContextOptions<PawnshopContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            new BranchLocationMapping().MapEntity(builder.Entity<BranchLocation>());
            new BranchMapping().MapEntity(builder.Entity<Branch>());
            new BranchWorkTimeMapping().MapEntity(builder.Entity<BranchWorkTime>());
            new ClientLocationMapping().MapEntity(builder.Entity<ClientLocation>());
            new ClientMapping().MapEntity(builder.Entity<Client>());
            new ClientOperationHistoryMapping().MapEntity(builder.Entity<ClientOperationHistory>());
            new CompanyMapping().MapEntity(builder.Entity<Company>());
            new ContractMapping().MapEntity(builder.Entity<Contract>());
            new EmployeeMapping().MapEntity(builder.Entity<Employee>());
            new EmployeeOperationHistoryMapping().MapEntity(builder.Entity<EmployeeOperationHistory>());
            new HistoryRecordMapping().MapEntity(builder.Entity<HistoryRecord>());
            new HistoryRecordTypeMapping().MapEntity(builder.Entity<HistoryRecordType>());
            new PawnItemMapping().MapEntity(builder.Entity<PawnItem>());
            new CompanyReviewMapping().MapEntity(builder.Entity<CompanyReview>());

            new IdentityUserClaimMapping().MapEntity(builder.Entity<IdentityUserClaim>());
            new IdentityRoleClaimMapping().MapEntity(builder.Entity<IdentityRoleClaim>());
            new IdentityRoleMapping().MapEntity(builder.Entity<IdentityRole>());
            new IdentityUserLoginMapping().MapEntity(builder.Entity<IdentityUserLogin>());
            new IdentityUserMapping().MapEntity(builder.Entity<IdentityUser>());
            new IdentityUserRoleMapping().MapEntity(builder.Entity<IdentityUserRole>());

            base.OnModelCreating(builder);
        }
    }
}
