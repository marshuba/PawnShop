﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Context.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.IdentityMapping
{
    public class IdentityUserLoginMapping : IMapEntity<IdentityUserLogin>
    {
        public void MapEntity(EntityTypeBuilder<IdentityUserLogin> builder)
        {
            builder.ForNpgsqlToTable("user_logins", "identity");
            builder.HasKey(entity => new { entity.LoginProvider, entity.ProviderKey });
            builder.Property(entity => entity.LoginProvider).HasColumnName("login_provider").IsRequired();
            builder.Property(entity => entity.ProviderKey).HasColumnName("provider_key")
                .IsRequired();
            builder.Property(entity => entity.ProviderDisplayName).HasColumnName("provider_display_name");
            builder.Property(entity => entity.UserId).HasColumnName("user_id")
                .IsRequired();
        }
    }
}
