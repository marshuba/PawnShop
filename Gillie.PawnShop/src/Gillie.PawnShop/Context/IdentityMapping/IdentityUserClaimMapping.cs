﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Context.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.IdentityMapping
{
    public class IdentityUserClaimMapping : IMapEntity<IdentityUserClaim>
    {
        public void MapEntity(EntityTypeBuilder<IdentityUserClaim> builder)
        {
            builder.ForNpgsqlToTable("user_claims", "identity");
            builder.HasKey(entity => entity.Id);
            builder.Property(entity => entity.Id).HasColumnName("user_claim_id");
            builder.Property(entity => entity.ClaimType).HasColumnName("claim_type").IsRequired();
            builder.Property(entity => entity.ClaimValue).HasColumnName("claim_value");
            builder.Property(entity => entity.UserId).HasColumnName("user_id");
        }
    }
}
