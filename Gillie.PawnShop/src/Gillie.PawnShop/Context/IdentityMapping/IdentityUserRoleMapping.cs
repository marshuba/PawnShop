﻿using Gillie.PawnShop.Authorization.Entities.Identity;
using Gillie.PawnShop.Context.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.IdentityMapping
{
    public class IdentityUserRoleMapping : IMapEntity<IdentityUserRole>
    {
        public void MapEntity(EntityTypeBuilder<IdentityUserRole> builder)
        {
            builder.ForNpgsqlToTable("users_roles", "identity");
            builder.HasKey(entity => new { entity.UserId, entity.RoleId });
            builder.Property(entity => entity.UserId).HasColumnName("user_id").IsRequired();
            builder.Property(entity => entity.RoleId).HasColumnName("role_id").IsRequired();
        }
    }
}
