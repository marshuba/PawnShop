﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class ContractMapping : IMapEntity<Contract>
    {
        public void MapEntity(EntityTypeBuilder<Contract> builder)
        {
            builder.ForNpgsqlToTable("contracts");
            builder.HasKey(contract => contract.ContractId);

            builder.HasOne(contract => contract.Client);
            builder.HasOne(contract => contract.Employee)
                .WithMany(employee => employee.Contracts)
                .HasForeignKey(contract => contract.ContractId);
            builder.HasOne(contract => contract.PawnItem);

            builder.Property(contract => contract.ContractId).HasColumnName("id");
            builder.Property(contract => contract.Percents).HasColumnName("percents");
            builder.Property(contract => contract.DateStart).HasColumnName("datastart");
            builder.Property(contract => contract.DateEnd).HasColumnName("dataend");
            builder.Property(contract => contract.IsClosed).HasColumnName("isclosed");
            builder.Property(contract => contract.EmployeeId).HasColumnName("idemployee");
            builder.Property(contract => contract.ClientId).HasColumnName("idclient");
        }
    }
}
