﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class EmployeeMapping : IMapEntity<Employee>
    {
        public void MapEntity(EntityTypeBuilder<Employee> builder)
        {
            builder.ForNpgsqlToTable("employees");
            builder.HasKey(employee => employee.EmployeeId);

            builder.HasMany(employee => employee.Contracts);
            builder.HasMany(employee => employee.EmployeeOperationHistories);
            builder.HasOne(employee => employee.Company);

            builder.Property(employee => employee.EmployeeId).HasColumnName("id");
            builder.Property(employee => employee.Name).HasColumnName("name");
            builder.Property(employee => employee.Surname).HasColumnName("surname");
            builder.Property(employee => employee.Email).HasColumnName("email");
            builder.Property(employee => employee.PhoneNumber).HasColumnName("phonenumber");
            builder.Property(employee => employee.CompanyId).HasColumnName("idcompany");
            builder.Property(employee => employee.EmployeeType).HasColumnName("type");
        }
    }
}
