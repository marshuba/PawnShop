﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class BranchLocationMapping : IMapEntity<BranchLocation>
    {
        public void MapEntity(EntityTypeBuilder<BranchLocation> builder)
        {
            builder.ForNpgsqlToTable("branchlocation");
            builder.HasKey(branchLocation => branchLocation.BranchLocationId);

            builder.HasOne(branchLocation => branchLocation.Branch)
                .WithOne(branch => branch.BranchLocation);

            builder.Property(branchLocation => branchLocation.BranchId).HasColumnName("id");
            builder.Property(branchLocation => branchLocation.Address).HasColumnName("address");
            builder.Property(branchLocation => branchLocation.City).HasColumnName("city");
            builder.Property(branchLocation => branchLocation.State).HasColumnName("state");
            builder.Property(branchLocation => branchLocation.PostalCode).HasColumnName("postalcode");
            builder.Property(branchLocation => branchLocation.BranchId).HasColumnName("idbranch");
        }
    }
}
