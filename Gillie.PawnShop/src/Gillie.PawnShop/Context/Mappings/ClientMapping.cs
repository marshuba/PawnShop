﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class ClientMapping : IMapEntity<Client>
    {
        public void MapEntity(EntityTypeBuilder<Client> builder)
        {
            builder.ForNpgsqlToTable("clients");
            builder.HasKey(client => client.ClientId);

            builder.HasOne(client => client.ClientLocation)
                .WithOne(clientLocation => clientLocation.Client)
                .HasForeignKey<ClientLocation>(clientLocation => clientLocation.ClientId);

            builder.HasMany(client => client.Reviews);
            builder.HasMany(client => client.ClientOperationHistories);
            builder.HasMany(client => client.Contracts);

            builder.Property(client => client.ClientId).HasColumnName("id");
            builder.Property(client => client.Name).HasColumnName("name");
            builder.Property(client => client.Surname).HasColumnName("surname");
            builder.Property(client => client.Email).HasColumnName("email");
            builder.Property(client => client.PhoneNumber).HasColumnName("phonenumber");
        }
    }
}
