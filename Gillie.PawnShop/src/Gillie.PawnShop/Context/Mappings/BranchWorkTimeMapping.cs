﻿using System;
using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class BranchWorkTimeMapping : IMapEntity<BranchWorkTime>
    {
        public void MapEntity(EntityTypeBuilder<BranchWorkTime> builder)
        {
            builder.ForNpgsqlToTable("branchworktimes");
            builder.HasKey(branchWorkTime => branchWorkTime.BranchId);

            builder.HasOne(branchWorkTime => branchWorkTime.Branch);

            builder.Property(branchWorkTime => branchWorkTime.BranchId).HasColumnName("id");
            builder.Property(branchWorkTime => branchWorkTime.BranchId).HasColumnName("begintime");
            builder.Property(branchWorkTime => branchWorkTime.BranchId).HasColumnName("endtime");
            builder.Property(branchWorkTime => branchWorkTime.BranchId).HasColumnName("day");
            builder.Property(branchWorkTime => branchWorkTime.BranchId).HasColumnName("idbranch");
        }
    }
}
