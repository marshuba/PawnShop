﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class CompanyReviewMapping : IMapEntity<CompanyReview>
    {
        public void MapEntity(EntityTypeBuilder<CompanyReview> builder)
        {
            builder.ForNpgsqlToTable("reviews");
            builder.HasKey(review => review.CompanyReviewId);

            builder.HasOne(review => review.Client);

            builder.Property(review => review.CompanyReviewId).HasColumnName("id");
            builder.Property(review => review.ReviewDate).HasColumnName("date");
            builder.Property(review => review.Description).HasColumnName("description");
            builder.Property(review => review.ClientId).HasColumnName("idclient");
            builder.Property(review => review.Publish).HasColumnName("publish");
        }
    }
}
