﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class EmployeeOperationHistoryMapping : IMapEntity<EmployeeOperationHistory>
    {
        public void MapEntity(EntityTypeBuilder<EmployeeOperationHistory> builder)
        {
            builder.ForNpgsqlToTable("employeeoperationhistories");
            builder.HasKey(empOpHistory => empOpHistory.EmployeeOperationHistoryId);

            builder.HasOne(empOpHistory => empOpHistory.Employee);

            builder.Property(empOpHistory => empOpHistory.EmployeeOperationHistoryId).HasColumnName("id");
            builder.Property(empOpHistory => empOpHistory.Name).HasColumnName("name");
            builder.Property(empOpHistory => empOpHistory.DateOperation).HasColumnName("date");
            builder.Property(empOpHistory => empOpHistory.Description).HasColumnName("description");
            builder.Property(empOpHistory => empOpHistory.EmployeeId).HasColumnName("idemployee");
            builder.Property(empOpHistory => empOpHistory.EmployeeOperationType).HasColumnName("type");
        }
    }
}
