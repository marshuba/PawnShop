﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class HistoryRecordMapping : IMapEntity<HistoryRecord>
    {
        public void MapEntity(EntityTypeBuilder<HistoryRecord> builder)
        {
            builder.ForNpgsqlToTable("historyrecords");
            builder.HasKey(historyRecord => historyRecord.HistoryRecordId);

            builder.HasOne(historyRecord => historyRecord.HistoryRecordType)
                .WithMany(historyRecordType => historyRecordType.HistoryRecords)
                .HasForeignKey(historyRecordType => historyRecordType.HistoryRecordId);

            builder.Property(historyRecord => historyRecord.HistoryRecordId).HasColumnName("id");
            builder.Property(historyRecord => historyRecord.Name).HasColumnName("name");
            builder.Property(historyRecord => historyRecord.HistoryRecordDate).HasColumnName("date");
            builder.Property(historyRecord => historyRecord.Description).HasColumnName("description");
            builder.Property(historyRecord => historyRecord.CompanyId).HasColumnName("idcompany");
            builder.Property(historyRecord => historyRecord.HistoryRecordTypeId).HasColumnName("idhistoryrecordtype");
        }
    }
}
