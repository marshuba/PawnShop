﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class ClientLocationMapping : IMapEntity<ClientLocation>
    {
        public void MapEntity(EntityTypeBuilder<ClientLocation> builder)
        {
            builder.ForNpgsqlToTable("clientlocations");
            builder.HasKey(clientLocation => clientLocation.ClientLocationId);

            builder.HasOne(clientLocation => clientLocation.Client)
                .WithOne(client => client.ClientLocation);

            builder.Property(clientLocation => clientLocation.ClientLocationId).HasColumnName("id");
            builder.Property(clientLocation => clientLocation.Address).HasColumnName("address");
            builder.Property(clientLocation => clientLocation.City).HasColumnName("city");
            builder.Property(clientLocation => clientLocation.State).HasColumnName("state");
            builder.Property(clientLocation => clientLocation.PostalCode).HasColumnName("postalcode");
            builder.Property(clientLocation => clientLocation.ClientId).HasColumnName("idclient");
        }
    }
}
