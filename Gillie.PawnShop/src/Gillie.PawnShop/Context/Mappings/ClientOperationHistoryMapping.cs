﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class ClientOperationHistoryMapping : IMapEntity<ClientOperationHistory>
    {
        public void MapEntity(EntityTypeBuilder<ClientOperationHistory> builder)
        {
            builder.ForNpgsqlToTable("clientoperationhistories");
            builder.HasKey(opHistory => opHistory.ClientOperationHistoryId);

            builder.HasOne(opHistory => opHistory.Client);

            builder.Property(opHistory => opHistory.ClientOperationHistoryId).HasColumnName("id");
            builder.Property(opHistory => opHistory.DateOperation).HasColumnName("date");
            builder.Property(opHistory => opHistory.Name).HasColumnName("name");
            builder.Property(opHistory => opHistory.Description).HasColumnName("description");
            builder.Property(opHistory => opHistory.ClientId).HasColumnName("idclient");
            builder.Property(opHistory => opHistory.ClientOperationType).HasColumnName("type");
        }
    }
}
