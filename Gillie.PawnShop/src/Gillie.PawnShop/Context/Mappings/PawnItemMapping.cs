﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class PawnItemMapping : IMapEntity<PawnItem>
    {
        public void MapEntity(EntityTypeBuilder<PawnItem> builder)
        {
            builder.ForNpgsqlToTable("items");
            builder.HasKey(item => item.PawnItemId);

            builder.HasOne(item => item.Contract);

            builder.Property(item => item.PawnItemId).HasColumnName("id");
            builder.Property(item => item.Name).HasColumnName("name");
            builder.Property(item => item.Description).HasColumnName("description");
            builder.Property(item => item.PawnItemState).HasColumnName("state");
            builder.Property(item => item.BuyPrice).HasColumnName("buyprice");
            builder.Property(item => item.SellPrice).HasColumnName("sellprice");
            builder.Property(item => item.ContractId).HasColumnName("idcontract");
        }
    }
}
