﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class CompanyMapping : IMapEntity<Company>
    {
        public void MapEntity(EntityTypeBuilder<Company> builder)
        {
            builder.ForNpgsqlToTable("company");
            builder.HasKey(company => company.CompanyId);

            builder.HasMany(company => company.Branches)
                .WithOne(branch => branch.Company)
                .HasForeignKey(branch => branch.CompanyId);

            builder.HasMany(company => company.HistoryRecordes);
            builder.HasMany(company => company.Employees);

            builder.Property(company => company.CompanyId).HasColumnName("id");
            builder.Property(company => company.Name).HasColumnName("name");
            builder.Property(company => company.Address).HasColumnName("address");
        }
    }
}
