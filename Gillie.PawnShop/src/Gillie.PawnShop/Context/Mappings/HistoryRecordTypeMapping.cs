﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class HistoryRecordTypeMapping : IMapEntity<HistoryRecordType>
    {
        public void MapEntity(EntityTypeBuilder<HistoryRecordType> builder)
        {
            builder.ForNpgsqlToTable("historyrecordtype");
            builder.HasKey(historyRecordType => historyRecordType.HistoryRecordTypeId);

            builder.HasMany(historyRecordType => historyRecordType.HistoryRecords)
                .WithOne(historyRecord => historyRecord.HistoryRecordType)
                .HasForeignKey(historyRecord => historyRecord.HistoryRecordTypeId);

            builder.Property(historyRecordType => historyRecordType.HistoryRecordTypeId).HasColumnName("id");
            builder.Property(historyRecordType => historyRecordType.Name).HasColumnName("type");
        }
    }
}
