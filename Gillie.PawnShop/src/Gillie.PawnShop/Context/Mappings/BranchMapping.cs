﻿using Gillie.PawnShop.Context.Interface;
using Gillie.PawnShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Mappings
{
    public class BranchMapping : IMapEntity<Branch>
    {
        public void MapEntity(EntityTypeBuilder<Branch> builder)
        {
            builder.ForNpgsqlToTable("branches");
            builder.HasKey(branch => branch.BranchId);

            builder.HasOne(branch => branch.BranchLocation)
                .WithOne(location => location.Branch)
                .HasForeignKey<BranchLocation>(location => location.BranchId);

            builder.HasOne(branch => branch.Company)
                .WithMany(company => company.Branches)
                .HasForeignKey(company => company.CompanyId);

            builder.HasMany(branch => branch.BranchWorkTimes)
                .WithOne(branchWorkTime => branchWorkTime.Branch)
                .HasForeignKey(branchWorkTime => branchWorkTime.BranchId);

            builder.Property(branch => branch.BranchId).HasColumnName("id");
            builder.Property(branch => branch.Name).HasColumnName("name");
            builder.Property(branch => branch.PhoneNumber).HasColumnName("phonenumber");
            builder.Property(branch => branch.CompanyId).HasColumnName("idcompany");
            builder.Property(branch => branch.BranchType).HasColumnName("type");
        }
    }
}
