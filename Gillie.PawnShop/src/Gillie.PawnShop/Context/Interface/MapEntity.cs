﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Gillie.PawnShop.Context.Interface
{
    public interface IMapEntity<T> where T : class
    {
        void MapEntity(EntityTypeBuilder<T> builder);
    }
}