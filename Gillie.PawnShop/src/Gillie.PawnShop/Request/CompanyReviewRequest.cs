﻿namespace Gillie.PawnShop.Request
{
    public class CompanyReviewRequest
    {
        public string Description { get; set; }
    }
}