﻿namespace Gillie.PawnShop.Request
{
    public class ClientRequest
    {
        public long ClientId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
