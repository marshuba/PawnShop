﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Request
{
    public class ContractRequest
    {
        public long ClientId { get; set; }
        public long EmployeeId { get; set; }
        public double Percent { get; set; }
        public DateTime DateEnd { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public double BuyPrice { get; set; }
        public double SellPrice { get; set; }
    }
}
