﻿using System.ComponentModel.DataAnnotations;

namespace Gillie.PawnShop.Request
{
    public class RegisterRequest
    {
        [Required]
        [Display(Name = "Your E-mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Your Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Your Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Your Surname")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Your Phone number")]
        public string PhoneNumber { get; set; }
    }
}
