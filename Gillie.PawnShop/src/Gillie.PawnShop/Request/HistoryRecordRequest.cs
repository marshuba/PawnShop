﻿using System;

namespace Gillie.PawnShop.Request
{
    public class HistoryRecordRequest
    {      
        public long HistoryRecordId { get; set; }
        public long HistoryRecordTypeId { get; set; }
        public long CompanyId { get; set; }
        public DateTime HistoryRecordDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
