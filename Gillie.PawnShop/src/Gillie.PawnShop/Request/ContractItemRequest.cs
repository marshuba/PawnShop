﻿using System;

namespace Gillie.PawnShop.Request
{
    public class ContractItemRequest
    {
        public long ClientId { get; set; }
        public long EmployeeId { get; set; }
        public long PawnItemId { get; set; }

        public double Percents { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool IsClosed { get; set; } = false;
    }
}
