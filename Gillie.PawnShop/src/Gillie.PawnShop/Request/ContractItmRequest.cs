﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gillie.PawnShop.Request
{
    public class ContractItmRequest
    {
        public DateTime EndDate { get; set; }
        public double Percents { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public double BuyPrice { get; set; }
        public double SellPrice { get; set; }
    }
}