﻿using Gillie.PawnShop.Entities.Enums;

namespace Gillie.PawnShop.Request
{
    public class BranchRequest
    {
        public long BranchId { get; set; }
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public BranchType BranchType { get; set; }
    }
}
