﻿using System;

namespace Gillie.PawnShop.Request
{
    public class EmployeeProfitReportRequest
    {
        public string Email { get; set; }
        public DateTime FirstDate { get; set; }
        public DateTime LastDate { get; set; }
    }
}